// graceUI/components/graceSwiper.js
Component({
  properties: {
    width:{ type : Number, value : 750 },
		height:{ type : Number, value : 300 },
		swiperItems : { type : Array, value : new Array() },
		borderRadius : { type : String, value : '10rpx'},
		indicatorBarHeight:{type : Number, value : 68},
		indicatorBarBgColor:{type : String, value : 'rgba(0,0,0,0)'},
		indicatorWidth : { type:Number, value:18 },
		indicatorActiveWidth :{ type:Number, value:18 },
		indicatorHeight : { type:Number, value:18 },
		indicatorRadius:{ type:Number, value:18 },
		indicatorColor : { type : String, value : "rgba(255, 255, 255, 0.6)" },
		indicatorActiveColor : { type : String, value : "#3688FF" },
    indicatorType:{ type : String, value : "dot" },
    indicatorPosition:{ type : String, value : "absolute" },
    indicatorDirection:{type:String, value:'center'},
		spacing : { type : Number, value : 50 },
		padding : { type : Number, value : 26 },
		interval : { type : Number, value : 5000 },
		autoplay : { type : Boolean, value : true },
		currentIndex : { type : Number, value : 0 },
		opacity:{ type : Number, value:0.66},
		titleColor:{type:String, value:"#FFFFFF"},
    titleSize:{type:String, value:"28rpx"}
  },
  data: {
    widthIn : 750,
    heightIn  : 300,
    widthInSamll:700,
    heightInSmall:280,
    paddingY:0,
    currentIndex : 0
  },
  ready:function(){
    this.setData({current : this.data.currentIndex});
		// 图片宽高计算
    this.data.widthIn   = this.data.width - this.data.spacing*2;
    this.data.heightIn  = this.data.height / this.data.width * this.data.widthIn;
    this.data.paddingY  = this.data.padding * this.data.height / this.data.width;
    this.data.widthInSamll  = this.data.widthIn -  this.data.padding * 2;
    this.data.heightInSmall = this.data.heightIn - this.data.paddingY * 2;
    this.setData({
      widthIn : this.data.widthIn,
      heightIn : this.data.heightIn,
      paddingY : this.data.paddingY,
      widthInSamll : this.data.widthInSamll,
      heightInSmall : this.data.heightInSmall
    });
    watch(this, {
      currentIndex: function (vn, vo) {
       this.setData({current : vn});
      }
    });
  },
  methods: {
    swiperchange: function (e) {
      var currentVal =  e.detail.current;
      this.setData({current : currentVal});
      this.triggerEvent('swiperchange',currentVal);
    },
    taped : function(e){
      this.triggerEvent('taped', e.currentTarget.dataset.index);
    }
  }
});
function defineReactive(data, key, val, fn) {
  Object.defineProperty(data, key, {
    configurable: true,
    enumerable: true,
    get: function () {return val;},
    set: function (newVal) {
      if (newVal === val) return
      fn && fn(newVal)
      val = newVal
    }
  })
}
function watch(ctx, obj) {
  Object.keys(obj).forEach(key => {
    defineReactive(ctx.data, key, ctx.data[key], function (value) {obj[key].call(ctx, value);});
  });
}
