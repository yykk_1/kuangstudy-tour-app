Component({
  properties: {
    itemWidth: { type: String, value: "200rpx" },
    type: { type: String, value : "" },
    items: { type: Array, value : [] },
    fontSize: { type: String, value: "26rpx" },
    selectedColor : {type : String, value : "#3688FF"},
    bgColor :  { type: String, value: "#F6F7F8"},
		height:{type: String, value:"68rpx"},
		borderRadius : {type: String, value:"10rpx"},
		fontColor:{type: String, value:"#323232"},
    fontActiveColor:{type: String, value:"#FFFFFF"}
  },
  data: {
    tagsData: []
  },
  ready : function(){
    if(this.data.items == null){
      this.setData({tagsData : []});
    }else{
      this.setData({tagsData : this.data.items});
      this.initChange();
    }
    watch(this,{
      items : function(nv, ov){
        this.setData({tagsData : nv});
        this.initChange();
      }
    });
  },
  methods: {
    initChange : function () {
			if(this.data.type == 'radio'){
				var selectVal   = '';
				for (var i = 0; i < this.data.tagsData.length; i++){
					if(this.data.tagsData[i].checked){selectVal   = this.data.tagsData[i].value;}
				}
        //this.triggerEvent("change", selectVal);
			}else{
				var sedRes = [];
				for (var i = 0; i < this.data.tagsData.length; i++){
					if(this.data.tagsData[i].checked){
						sedRes.push(this.data.tagsData[i].value);
					}
        }
        //this.triggerEvent("change", sedRes);
			}
		},
    graceSelectChange: function (index) {
      if (index.currentTarget) { index = index.currentTarget.dataset.index;}
      // 单选
      if (this.data.type == 'radio') {
        for (var i = 0; i < this.data.tagsData.length; i++) { this.data.tagsData[i].checked = false; }
        this.data.tagsData[index].checked = true;
        this.setData({ tagsData: this.data.tagsData });
        this.triggerEvent("change", this.data.tagsData[index].value);
      }
      // 多选
      else {
        if (this.data.tagsData[index].checked) {
          this.data.tagsData[index].checked = false;
        } else {
          this.data.tagsData[index].checked = true;
        }
        this.setData({ tagsData: this.data.tagsData });
        var sedRes = [];
        for (var i = 0; i < this.data.tagsData.length; i++) {
          if (this.data.tagsData[i].checked) {
            sedRes.push(this.data.tagsData[i].value);
          }
        }
        this.setData({ tagsData: this.data.tagsData });
        this.triggerEvent("change", sedRes);
      }
    },
    setItems : function(items){
      this.setData({tagsData : items});
      this.initChange();
    },
    clearSelected:function(){
			var newData = [];
			for(let i = 0; i < this.data.tagsData.length; i++){
				this.data.tagsData[i].checked = false;
				newData.push(this.data.tagsData[i]);
			}
      this.setData({tagsData:newData});
			if(this.data.type == 'radio'){
        this.triggerEvent("change", '');
      }else{
        this.triggerEvent("change", []);
      }
		},
		selectAll : function () {
			if(this.data.type == 'radio'){return ;}
			var newData = [], reDatas = [];
			for(let i = 0; i < this.data.tagsData.length; i++){
				this.data.tagsData[i].checked = true;
				newData.push(this.data.tagsData[i]);
				reDatas.push(this.data.tagsData[i].value);
			}
      this.setData({tagsData:newData});
			this.triggerEvent("change", reDatas);
		}
  }
});
function defineReactive(data, key, val, fn) {
  Object.defineProperty(data, key, {
    configurable: true,
    enumerable: true,
    get: function () {return val},
    set: function (newVal) {
      if (newVal === val) return;
      fn && fn(newVal);
      val = newVal;
    },
  })
}
function watch(ctx, obj) {
  Object.keys(obj).forEach(key => {
    defineReactive(ctx.data, key, ctx.data[key], function (value) {obj[key].call(ctx, value)});
  })
}
