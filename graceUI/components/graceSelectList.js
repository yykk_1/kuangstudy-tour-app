// graceUI/components/graceSelectList.js
Component({
  properties: {
    lists : {type:Array, value:[]},
		checkColor:{type:String, value:"#3688FF"},
		type : { type: String, value: "radio"},
		imgSize:{type:Array, value:['68rpx','68rpx']},
		fontSize:{type: String, value: "28rpx"},
		isBorder:{type:Boolean, value:true},
		lineHeight:{type: String, value: "50rpx"},
		itemColor:{type:String, value:"#323232"},
		imgBorderRadius:{type: String, value: "60rpx"},
		borderColor:{type:String, value:"#F6F6F6"},
		maxSize:{type:Number,value:0}
  },
  methods: {
    choose:function(e){
			var index = e.currentTarget.dataset.index;
			if(this.data.type == 'radio'){
				if(this.data.lists[index].checked){
          this.data.lists[index].checked = false;
          this.setData({lists:this.data.lists});
					this.triggerEvent('change', -1);
				}else{
					for(let i = 0; i < this.data.lists.length; i++){
            this.data.lists[i].checked = false;
					}
          this.data.lists[index].checked = true;
          this.setData({lists:this.data.lists});
          this.triggerEvent('change', index);
        }
			}else{
				if(this.data.lists[index].checked){
					this.data.lists[index].checked = false;
				}else{
					if(this.data.maxSize > 0){
						var size = 0;
						this.data.lists.forEach((item)=>{
							if(item.checked){size++;}
						});
						size++;
						if(size > this.data.maxSize){this.triggerEvent('maxSed'); return ;}
					}
					this.data.lists[index].checked = true;
				}
				var sedArr = [];
				for(let i = 0; i < this.data.lists.length; i++){
					if(this.data.lists[i].checked){
						sedArr.push(i);
					}
				}
				this.setData({lists:this.data.lists});
        this.triggerEvent('change', sedArr);
			}
		}
  }
})
