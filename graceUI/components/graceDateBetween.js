// graceUI/components/graceDateBetween.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    weexBg:{type:String, value:'#F8F8F8'},
		unit:{type:Array,value:[' 年 ',' 月']},
		sedBg:{type:String, value:'rgba(54,136,255,0.8)'},
		itemBg:{type:String, value:'#FFFFFF'},
		color:{type:String, value:'#323232'},
		sedColor:{type:String, value:'#FFFFFF'},
		startBg:{type:String, value:'rgba(54,136,255,1)'},
		endBg:{type:String,value:'rgba(54,136,255,1)'},
		monthNumber:{type:Number,value:2}
  },
  /**
   * 组件的初始数据
   */
  data: {
		nextdays:[],
		sedDays:[0,0],
		btDays :[],
		nextbtDays:[],
		daysData : []
  },
  ready:function(){
    this.setMonth(this.currentMonth());
  },
  methods:{
		setBetween:function(days){
			if(days[1] == 0){days[1] = days[0];}
			days[0] = Number(days[0]);
			days[1] = Number(days[1]);
			this.setData({sedDays : days});
			var countNumber = 0;
			var daysNew = [];
			this.data.daysData.forEach((itm)=>{
				var year  = itm[0][0];
				var month = itm[0][1];
				var daysIn = [];
				itm[1].forEach((item)=>{
					var cDay = year+''+month+''+item[0];
					cDay = Number(cDay);
					if(cDay >= days[0] && cDay <= days[1]){
						item[1] = true; 
						countNumber++;
					}else{
						item[1] = false;
					}
					daysIn.push(item);
				});
				daysNew.push([[year, month], daysIn]);
			});
			this.setData({daysData : daysNew});
			this.triggerEvent('selectDate', [days, countNumber]);
		},
		setMonth:function(month){
			var reg = /^([0-9]{4}).*([0-9]{2}).*$/;
			var res = month.match(reg);
			if(res == null){month = this.currentMonth(); res = month.match(reg);}
			this.setMonthBase(res);
		},
		setMonthBase : function(res){
			var daysData = [];
			if(res[2].substr(0,1) == '0'){res[2] = res[2].substr(1);}
			res[1] = Number(res[1]);
			res[2] = Number(res[2]);
			var year  = res[1];
			var month = res[2];
			for(let i = 0; i < this.data.monthNumber; i++){
				var monthIn = month + i;
				var yearIn  = year;
				if(monthIn > 12){monthIn = monthIn - 12; yearIn += 1;}
				if(monthIn < 10){monthIn = '0'+monthIn;}
				daysData[i] = [];
				daysData[i].push([yearIn, monthIn]);
				var days = this.getDays(yearIn,monthIn);
				var daysList  = [];
				for (let ii = (0 - days[1]); ii < days[0]; ii++){
					if(ii >= 0){
						daysList.push([ii >= 9 ? ii + 1 : '0' + (ii+1), false]);
					}else{
						daysList.push(['',false]);
					}
				}
				daysData[i].push(daysList);
			}
			this.setData({daysData : daysData});
		},
		currentMonth : function () {
			var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth() + 1;
			m = m < 10 ? ('0' + m) : m;
			return y + '年' + m + '月';
		},
		getDays : function(year,month){
			var d    = new Date(year, month, 0);
			var days = d.getDate();
			var d2   = new Date(year, month - 1, 0);
			var startWeek = d2.getDay();
			return [days, startWeek];
		},
		selectDay:function(e){
			var day = e.currentTarget.dataset.dt;
			if(day == ''){return ;}
			day = Number(day);
			if(this.data.sedDays[0] == 0){ this.data.sedDays[0] = day; }
			else if(this.data.sedDays[1] == 0 || this.data.sedDays[0] == this.data.sedDays[1]){
				if(day > this.data.sedDays[0]){
					this.data.sedDays[1] = day;
				}else if(day < this.data.sedDays[0]){
					this.data.sedDays[1] = this.data.sedDays[0];
					this.data.sedDays[0] = day;
				}
			}else{
				this.data.sedDays[0] = day;
				this.data.sedDays[1] = 0;
			}
			this.setData({sedDays : this.data.sedDays});
			this.setBetween(this.data.sedDays);
		}
	}
})
