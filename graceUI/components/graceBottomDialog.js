// graceUI/components/graceBottomDialog.js
Component({
  properties: {
    width:{type:String,value:'750rpx'},
		left:{type:String,value:'0rpx'},
    show: {
      type: Boolean,
      value: false
    },
    background: {
      type: String,
      value: 'rgba(0, 0, 0, 0.5)'
    },
    borderRadius: {
      type: String,
      value: '0rpx'
    },
    zIndex:{
      type: Number,
      value: 99
    },
    isSwitchPage:{type:Boolean, value:false}
  },
  data: {
    realShow: false,
    isIpx: false,
    isOut: false
  },
  methods: {
    closeDialog: function(){
      this.triggerEvent('closeDialog');
    },
    stopFun: function(){},
    open:function(){
      this.setData({realShow:true, isOut:false});
		},
		hide:function(){
      this.isOut = true;
      this.setData({isOut:true});
			setTimeout(()=>{this.setData({realShow:false});}, 120);
		}
  },
  options: {
    multipleSlots: true
  },
  ready: function () {
	  this.setData({realShow : this.data.show});
    var _self = this;
    wx.getSystemInfo({
      success: function (res) {
        res.model = res.model.replace(' ', '');
			  res.model = res.model.toLowerCase();
        var model = res.model;
        var res1 = model.indexOf('iphonex');
				if(res1 > 5){res1 = -1;}
				var res2 = model.indexOf('iphone1');
				if(res2 > 5){res2 = -1;}
				if(res1 != -1 || res2 != -1){
          _self.setData({ isIpx: true });
        }
      }
    });
    watch(this, {
      show: function (vn, vo) {
        if (vn) {
          this.setData({realShow : vn});
        } else {
          this.setData({ isOut: true });
          setTimeout(() => { this.setData({ realShow: false });}, 120);
          setTimeout(() => { this.setData({ isOut: false }); }, 150);
        }
      }
    });
  }
});
function defineReactive(data, key, val, fn) {
  Object.defineProperty(data, key, {
    configurable: true,
    enumerable: true,
    get: function () {
      return val
    },
    set: function (newVal) {
      if (newVal === val) return
      fn && fn(newVal)
      val = newVal
    },
  })
}

function watch(ctx, obj) {
  Object.keys(obj).forEach(key => {
    defineReactive(ctx.data, key, ctx.data[key], function (value) {
      obj[key].call(ctx, value)
    })
  })
}
