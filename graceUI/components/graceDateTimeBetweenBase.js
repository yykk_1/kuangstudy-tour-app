Component({
  properties: {
		value : { type : String , value:''},
		isTime : {type : Boolean, value : true},
		isSecond : {type : Boolean, value : true},
		startYear : {type : Number, value : 1980},
		endYear : {type : Number, value : 2050},
		units : {type : Array , value: ['年','月','日','时','分','秒']}
  },
  data: {
    show:false,
		defaultVal : [0,0,0,0,0,0],
		sDate:[[],[],[],[],[],[]]
  },
  ready:function(){
    this.init();
  },
  methods: {
		now : function () {
			var date = new Date();
			var y = date.getFullYear();
			var m = date.getMonth() + 1;
			m = m < 10 ? ('0' + m) : m;
			var d = date.getDate();
			d = d < 10 ? ('0' + d) : d;
			var h = date.getHours();
			h = h < 10 ? ('0' + h) : h;
			var minute = date.getMinutes();
			var second = date.getSeconds();
			minute = minute < 10 ? ('0' + minute) : minute;
			second = second < 10 ? ('0' + second) : second;
			return y + '-' + m + '-' + d + ' '+ h +':' + minute + ':' + second;
		},
		arrayIndexOf : function(arr, needFind){
			var index = -1;
			for(let i = 0; i < arr.length; i++){if(arr[i] == needFind){index = i; return i;}}
			return index;
		},
		setValue : function (val) {
      if(val == ''){val = this.now();}
			var reg = /^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})$/;
			var res = val.match(reg);
			if(res == null){
				reg = /^([0-9]{4})-([0-9]{2})-([0-9]{2})$/;
				res = val.match(reg);
				if(res == null){
					this.setValue(this.now());
					return ;
				}
				res[4] = '00';
				res[5] = '00';
				res[6] = '00';
      }
			this.setDefaults([res[1],res[2],res[3],res[4],res[5],res[6]]);
		},
		setDefaults : function (res) {
			for(let i = 0; i < res.length; i++){
				var index = this.arrayIndexOf(this.data.sDate[i], res[i]);
				if(index == -1){index = 0;}
				this.data.defaultVal.splice(i, 1, index);
      }
      this.setData({defaultVal : this.data.defaultVal});
			this.changeBase(this.data.defaultVal);
		},
		// 初始化组件
		init:function(){
			if(this.data.startYear < 1970){this.setData({startYear : 1970});}
			if(this.data.endYear < this.data.startYear){this.setData({endYear : this.data.startYear + 10});}
			var years     = new Array();
			for(let i = this.data.startYear; i <= this.data.endYear; i++){years.push(i);}
			var months     = new Array();
			for(let i = 1; i <= 12; i++){if(i < 10){months.push('0'+i);}else{months.push(i);}}
			var days     = new Array();
			for(let i = 1; i <= 31; i++){if(i < 10){days.push('0'+i);}else{days.push(i);}}
			var hours     =  new Array();
			for(let i = 0; i < 24; i++){if(i < 10){hours.push('0'+i);}else{hours.push(i);}}
			var minutes  =  new Array();
			var seconds  =  new Array();
			for(let i = 0; i < 60; i++){
				if(i < 10){minutes.push('0'+i); seconds.push('0'+i);}else{minutes.push(i); seconds.push(i);}
			}
			this.setData({sDate : [years, months, days, hours, minutes, seconds]}, function(){
				setTimeout(()=>{this.setValue(this.data.value);}, 500);
			});
		},
		change : function (res) {
			this.changeBase(res.detail.value);
		},
		changeBase:function(res){
			var date = new Date(this.data.sDate[0][res[0]], this.data.sDate[1][res[1]], 0);
			var days = date.getDate();
			var daysOut = new Array();
			for(let i = 1; i <= days; i++){if(i < 10){daysOut.push('0'+i);}else{daysOut.push(i);}}
			this.data.sDate.splice(2, 1, daysOut);
			if(res[2] + 1 > days){res[2] = days - 1;}
      this.setData({sDate : this.data.sDate, defaultVal : res});
			if(this.data.isTime){
				var resdata = new Array(this.data.sDate[0][this.data.defaultVal[0]],
				this.data.sDate[1][this.data.defaultVal[1]],
				this.data.sDate[2][this.data.defaultVal[2]],
				this.data.sDate[3][this.data.defaultVal[3]],
				this.data.sDate[4][this.data.defaultVal[4]],
				this.data.sDate[5][this.data.defaultVal[5]]);
			}else{
				var resdata = new Array(this.data.sDate[0][this.data.defaultVal[0]],
				this.data.sDate[1][this.data.defaultVal[1]],
				this.data.sDate[2][this.data.defaultVal[2]])
			}
			this.triggerEvent('change', resdata);
		},
		confirm:function () {
			if(this.data.isTime){
				var res = new Array(this.data.sDate[0][this.data.defaultVal[0]],
				this.data.sDate[1][this.data.defaultVal[1]],
				this.data.sDate[2][this.data.defaultVal[2]],
				this.data.sDate[3][this.data.defaultVal[3]],
				this.data.sDate[4][this.data.defaultVal[4]],
				this.data.sDate[5][this.data.defaultVal[5]]);
			}else{
				var res = new Array(this.data.sDate[0][this.data.defaultVal[0]],
				this.data.sDate[1][this.data.defaultVal[1]],
				this.data.sDate[2][this.data.defaultVal[2]])
			}
			this.triggerEvent('confirm', res);
			this.close();
		}
	}
})
