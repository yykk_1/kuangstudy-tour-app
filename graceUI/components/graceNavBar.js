Component({
  properties: {
    isCenter: {
      type: Boolean,
      value: false
    },
    currentIndex: {
      type: Number,
      value: 0
    },
    size: {
      type: Number,
      value: 120
    },
    items: {
      type: Array,
      value: function () {
        return []
      }
    },
    activeLineBg: {
      type: String,
      value: "linear-gradient(to right, #66BFFF,#3388FF)"
    },
    activeColor: {
      type: String,
      value: "#333333"
    },
    activeLineHeight: {
      type: String,
      value: '6rpx'
    },
    activeLineWidth: {
      type: String,
      value: "36rpx"
    },
    activeLineRadius : {type : String, value : "0rpx"},
    activeDirection: {
      type: String, 
      value: ""
    },
	  activeFontWeight: {type : Number, value : 900},
    color: {
      type: String,
      value: "#333333"
    },
    margin: {
      type: Number,
      value: 0
    },
    textAlign: {
      type: String,
      value: 'left'
    },
    lineHeight: {
      type: String,
      value: '50rpx'
    },
    fontSize: {
      type: String,
      value: '28rpx'
    },
    activeFontSize: { 
      type: String, 
      value: '28rpx' 
    },
    padding : {
      type: String,
      value: '0rpx'
    },
    animatie:{type:Boolean, value:true},
    autoLeft: {type : String, value : ''},
    scorllAnimation : { type: Boolean, value: true }
  },
  data:{
    scrollLeft     : 0,
		scrllTimer     : null
  },
  methods: {
    navchang: function (e) {
      this.triggerEvent('change', Number(e.currentTarget.dataset.index));
      if(this.data.isCenter){return ;}
			if(this.data.scrllTimer != null){clearTimeout(this.data.scrllTimer);}
			this.data.scrllTimer = setTimeout(()=>{this.setLeft();}, 200);
    },
    setLeft   : function () {
      var itemWidth = Number(this.data.size);
      itemWidth     = Number(itemWidth);
      var left      = (Number(this.data.currentIndex) + 1) * itemWidth - 350 - itemWidth / 2;
      var maxLeft   = Number(this.data.items.length) * itemWidth - 700;
			maxLeft       = (maxLeft - 30)/2;
      left          = left / 2;
      console.log(left)
			if(left < 0){left = 0;}
      if(left > maxLeft){left = maxLeft;}
      this.setData({scrollLeft:left});
		}
  }
})