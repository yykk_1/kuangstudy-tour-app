Component({
  properties: {
    items: {
      type: Array,
      value: []
    },
    show: {
      type: Boolean,
      value: false
    },
    height: {
      type: Number,
      value: 300
    },
    color: {
      type: String,
      value: "#333333"
    },
    activeColor: {
      type: String,
      value: "#3688FF"
    },
    selectIndex: {
      type: Number,
      value: 0
    },
    currentIndex : {
      type: Number,
      value: 0
    },
    padding:{
      type : String,
      value : "0 20rpx"
    },
    isInput:{type:Boolean, value:false},
		placeholder:{type:String, value:"自定义"},
		addBtnName:{type:String, value:"+ 添加"}
  },
  data: {
    top: 0,
    heightIn : 200,
    inputVal : ''
  },
  ready: function () {
    this.setData({currentIndex : this.data.selectIndex});
    watch(this, {
      selectIndex: function (newVal) {
        this.setData({currentIndex : newVal});
      }
    });
  },
  methods: {
    showMenu: function () {
      wx.createSelectorQuery().in(this).select('#menuMain').fields(
        { rect: true }, (res) => {
          this.setData({top:res.top});
          try {
            var system = wx.getSystemInfoSync();
					  var wHeight = system.windowHeight;
					  this.setData({heightIn  : wHeight - this.data.top});
          } catch (error) {}
        }
      ).exec();
      this.triggerEvent('showMenu');
    },
    close: function () {
      this.triggerEvent('close');
    },
    select: function (e) {
      var index = Number(e.currentTarget.dataset.index);
      this.setData({ currentIndex: index });
      this.triggerEvent('select', index);
      this.close();
    },
    stopFun:function(){},
    inputting : function(e){
      this.setData({inputVal:e.detail.value});
    },
    addTag : function () {
			if(this.data.inputVal == ''){return ;}
			this.triggerEvent('submit', this.data.inputVal);
			this.setData({inputVal:''});
		}
  }
});
function defineReactive(data, key, val, fn) {
  Object.defineProperty(data, key, {
    configurable: true,
    enumerable: true,
    get: function () {
      return val
    },
    set: function (newVal) {
      if (newVal === val) return
      fn && fn(newVal)
      val = newVal
    },
  })
}

function watch(ctx, obj) {
  Object.keys(obj).forEach(key => {
    defineReactive(ctx.data, key, ctx.data[key], function (value) {
      obj[key].call(ctx, value)
    })
  })
}