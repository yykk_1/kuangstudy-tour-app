// graceUI/components/gracePlayer.js
var gracePlayer;
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    color      : {type:String, value:'#FFFFFF'},
		list       : {type:Array , value:[]},
		listBg     : {type:String, value:'#292E35'},
		listHeight : {type:String, value:'880rpx'},
  },
  /**
   * 组件的初始数据
   */
  data: {
    playStatus  : 1,
    player      : null,
    playTime    : '00:00',
    timer       : null,
    dotype      : 1,
    index       : 0,
    listShow    : false,
    audioLength : 1
  },
  /**
   * 组件的方法列表
   */
  ready:function(){
    gracePlayer = wx.getBackgroundAudioManager();
		gracePlayer.onTimeUpdate(()=>{
			if(this.data.playStatus != 1){return ;}
			// 调整进度
			var progress = gracePlayer.currentTime / this.data.audioLength;
      progress = Math.round(progress * 100);
      this.selectComponent('#graceSingleSlider').setProgress(progress);
      this.setData({playTime:this.timeFormat(gracePlayer.currentTime)});
		});
		gracePlayer.onPlay(()=>{
			console.log('play');
      this.setData({playStatus:1, audioLength:gracePlayer.duration});
		});
		gracePlayer.onPause(()=>{
			console.log('pause');
      this.setData({playStatus:2});
		});
		gracePlayer.onEnded(()=>{
			console.log('ended');
			switch(this.data.dotype){
				case 1 :
				this.data.index++;
				if(this.data.index + 1 > this.data.list.length){
          this.setData({index:0});
        }else{
          this.setData({index:this.data.index});
        }
				this.play();
				break;
				case 2 :
				gracePlayer.seek(0);
				this.play();
				break;
			}
		});
		this.play();
  },
  methods: {
		play:function () {
			var audio               = this.data.list[this.data.index];
			gracePlayer.title       = audio.title;
			gracePlayer.singer      = audio.singer;
			gracePlayer.coverImgUrl = audio.coverImgUrl;
			gracePlayer.src         = audio.src;
			gracePlayer.play();
		},
		progressChange : function(e){
      e = e.detail;
			if(this.data.timer != null){
				clearTimeout(this.data.timer);
			}
			gracePlayer.pause();
			var needTime = this.data.audioLength * e / 100;
			needTime = Math.round(needTime);
      this.data.playTime = this.timeFormat(needTime);
			this.data.timer = setTimeout(()=>{
				gracePlayer.seek(needTime);
				gracePlayer.play();
      }, 800);
      this.setData({playTime : this.data.playTime});
		},
		timeFormat : function (s){
			s = Math.round(s);
			if(s < 60){
				if(s < 10){return '00:0'+s;}
				return '00:'+s;
			}else{
				var second = s % 60;
				s = s - second;
				var minute = s / 60;
				if(minute < 10){minute = '0'+minute;}
				if(second < 10){second = '0'+second;}
				return minute+':'+second;
			}
			
		},
		changeType : function () {
			switch(this.data.dotype){
				case 1 :
        this.setData({dotype:2});
				break;
				case 2 :
          this.setData({dotype:1});
				break;
			}
		},
		pause :function () {gracePlayer.pause();},
		playi :function () {gracePlayer.play();},
		next : function () {
			if(this.data.index + 1 >= this.data.list.length){
        wx.showToast({title:"已经到底了 (:", icon:"none"}); 
        return ;
      }
			this.setData({index : this.data.index + 1});
			this.play();
		},
		prev : function () {
			if(this.data.index -1 < 0){wx.showToast({title:"已经到头了 (:", icon:"none"}); return ;}
      this.setData({index : this.data.index - 1});
			this.play();
		},
		openList : function () { this.setData({listShow:true});},
		hideList : function () { this.setData({listShow:false}); },
		playList : function (e) {
			var idx = e.currentTarget.dataset.index;
      this.setData({index:idx});
			this.play();
		},
		setIndex : function (idx) { this.setData({index:idx}); }
  }
})
