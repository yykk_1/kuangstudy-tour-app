// graceUI/components/graceNavBar2.js
Component({
  properties: {
    isCenter: { type: Boolean, value: false },
    currentIndex: { type: Number, value: 0 },
    size: { type: Number, value: 138 },
    fontSize: { type: String, value: '28rpx' },
    activeFontSize: { type: String, value: '30rpx' },
    lineHeight: { type: Number, value: 52 },
    fontSizeSmall: { type: String, value: '22rpx' },
    lineHeightSamll: { type: Number, value: 28 },
    items: { type: Array, value:[]},
    color: { type: String, value: "#333333" },
    descColor: { type: String, value: "#999999" },
    activeColor: { type: String, value: "#3688FF" },
    bgColor: { type: String, value: '#FFFFFF' },
    autoLeft: {type : String, value : ''},
    scorllAnimation : { type: Boolean, value: true }
  },
  data:{
    scrollLeft     : 0,
		scrllTimer     : null
  },
  methods: {
    navchang: function (e) {
      this.triggerEvent('change', Number(e.currentTarget.dataset.index));
      if(this.data.isCenter){return ;}
			if(this.data.scrllTimer != null){clearTimeout(this.data.scrllTimer);}
			this.data.scrllTimer = setTimeout(()=>{this.setLeft();}, 200);
    },
    setLeft   : function () {
      var itemWidth = Number(this.data.size);
      itemWidth     = Number(itemWidth);
      var left      = (Number(this.data.currentIndex) + 1) * itemWidth - 350 - itemWidth / 2;
      var maxLeft   = Number(this.data.items.length) * itemWidth - 700;
			maxLeft       = (maxLeft - 30)/2;
      left          = left / 2;
      console.log(left)
			if(left < 0){left = 0;}
      if(left > maxLeft){left = maxLeft;}
      this.setData({scrollLeft:left});
		}
  }
})
