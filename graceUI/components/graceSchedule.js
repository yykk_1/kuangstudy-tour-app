import guiCalendar from './graceCalendar.js';
Component({
  properties: {
		currentDate   : {type:String, value:""},
		bgColor       : {type:String, value:"#F8F8F8"},
		activeBgColor : {type:String, value:"#3688FF"},
		isLunar       : {type:Boolean, value:true },
		startDate     : {type:String, value:'1950-01-01'},
		endDate       : {type:String, value:'2050-01-01'},
		schedules     : {type:Array,  value:[]},
		pointColor    : {type:String, value:"#FF0036"}
  },
  data: {
    cYear      : 2020,
    cMonth     : 1,
    cDay       : 10,
    cMonthStr  : '01',
    weeks      : ['一', '二', '三', '四', '五', '六', '日'],
    days       : [],
    currentDayIn : '',
    hours      : ['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'],
    schedulesIn: [] 
  },
  ready:function(){
    this.setData({currentDayIn:this.data.currentDate});
		this.initTime();
		this.getSchedulesIn();
  },
  methods: {
		initTime : function(){
			if(this.data.currentDayIn == ''){
				var dateObj        = new Date();
				this.data.cYear         = Number(dateObj.getFullYear());
				this.data.cMonth        = Number(dateObj.getMonth() + 1);
				this.data.cMonthStr     = this.data.cMonth < 10 ? '0' + this.data.cMonth : this.data.cMonth;
				this.data.cDay          = dateObj.getDate();
				this.data.cDay          = this.data.cDay < 10 ? '0' + this.data.cDay : this.data.cDay;
				this.data.currentDayIn  = this.data.cYear + '-' + this.data.cMonthStr + '-' + this.data.cDay;
			}else{
				var dates          = this.data.currentDayIn.split(' ');
				if (!dates[1]) { dates[1] = '';}
				var dayArr         = dates[0].split('-');
				this.data.cYear         = Number(dayArr[0]);
				this.data.cMonth        = dayArr[1];
				this.data.cDay          = dayArr[2];
				var reg            = new RegExp('^0[0-9]+$');
				if(reg.test(this.data.cMonth)){this.data.cMonth = this.data.cMonth.substr(1,1);}
				this.data.cMonth        = Number(this.data.cMonth);
				this.data.cMonthStr     = this.data.cMonth < 10 ? '0'+this.data.cMonth : this.data.cMonth;
				this.data.currentDayIn  = dates[0];
				this.data.currentTimeIn = dates[1];
      }
      this.setData({
        cYear : this.data.cYear,
        cMonth: this.data.cMonth,
        cMonthStr: this.data.cMonthStr,
        cDay: this.data.cDay,
        currentDayIn: this.data.currentDayIn
      });
      this.changeMonth();

		},
		changeMonth:function(){
			var daysList  = [];
			var days      = this.getDaysInOneMonth();
			var startWeek = this.getDay();
			var forSteps  = 0;
			for (var i = (0 - startWeek); i < days; i++){
				if(i >= 0){
					daysList[forSteps] = {date : i >= 9 ? i + 1 : '0' + (i+1), nl : ''};
					daysList[forSteps].nl = guiCalendar.getLunarInfo(this.data.cYear, this.data.cMonth, i + 1);
					daysList[forSteps].haveSe = this.haveSchedule(daysList[forSteps].date);
				}else{
					daysList[forSteps] = '';
				}
				forSteps++;
			}
      this.setData({days : daysList});
		},
		haveSchedule : function (day) {
			var cDay = this.data.cYear+'-'+this.data.cMonthStr+'-'+day;
			for(let i = 0; i < this.data.schedules.length; i++){
				if(this.data.schedules[i].datetime.indexOf(cDay) != -1){
					return true;
				}
			}
			return false;
		},
		getDaysInOneMonth : function (){
			var d = new Date(this.data.cYear, this.data.cMonth, 0);
			return d.getDate();
		},
		getDay : function (){
			var d = new Date(this.data.cYear, this.data.cMonth - 1, 0);
			return d.getDay();
		},
		selectDate : function(e){
      this.setData({currentDayIn : e.detail.value});
			this.initTime();
			this.getSchedulesIn();
			this.triggerEvent('selectDate', e.detail.value);
		},
		chooseDate: function (e) {
      this.setData({currentDayIn : e.currentTarget.dataset.cdate});
			this.getSchedulesIn();
			this.triggerEvent('chooseDate', e.currentTarget.dataset.cdate);
		},
		getSchedulesIn : function (){
			var res = [];
			for(let i = 0; i < this.data.hours.length; i++){
				var ctime = this.data.currentDayIn + ' ' + this.data.hours[i] + ':00';
				res.push([]);
				for(let ii = 0; ii < this.data.schedules.length; ii++){
					if(this.data.schedules[ii].datetime == ctime){
						res[i].push(this.data.schedules[ii]);
					}
				}
			}
      this.setData({schedulesIn : res});
		},
		scheduleTap : function (e) {
			var id = e.currentTarget.dataset.id;
			this.triggerEvent('scheduleTap', id);
		},
		gotoToday : function(){
			var dateObj             = new Date();
			this.data.cYear         = Number(dateObj.getFullYear());
			this.data.cMonth        = Number(dateObj.getMonth() + 1);
			this.data.cMonthStr     = this.data.cMonth < 10 ? '0' + this.data.cMonth : this.data.cMonth;
			this.data.cDay          = dateObj.getDate();
			this.data.cDay          = this.data.cDay < 10 ? '0' + this.data.cDay : this.data.cDay;
      this.data.currentDayIn  = this.data.cYear + '-' + this.data.cMonthStr + '-' + this.data.cDay;
      this.setData({
        cYear : this.data.cYear,
        cMonth: this.data.cMonth,
        cMonthStr: this.data.cMonthStr,
        cDay: this.data.cDay,
        currentDayIn: this.data.currentDayIn
      });
			this.changeMonth();
			this.getSchedulesIn();
			this.triggerEvent('gotoToday', this.data.currentDayIn);
		}
  }
})