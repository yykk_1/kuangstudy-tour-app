// graceUI/components/graceComment.js
Component({
  properties: {
    placeholder:{type:String, value:"说点什么吧"},
    isImg : {type:Boolean, value:true}
  },
  data: {
    comment:{img:'', content:'',at:''}
  },
  methods: {
		open : function () {this.selectComponent('#graceBottomDialog').open()},
		hide : function () {this.selectComponent('#graceBottomDialog').hide()},
		submit:function () {
			this.triggerEvent('submit', this.data.comment); 
			this.hide();
			this.setData({comment : {img:'', content:'',at:''}});
		},
		selectImg:function(){
			wx.chooseImage({
				count:1,
				success:(res)=>{
            this.data.comment.img = res.tempFilePaths[0];
            this.setData({comment : this.data.comment});
        }
			});
		},
		removeImg:function () {
      this.data.comment.img = '';
      this.setData({comment : this.data.comment});
		},
		setAt:function(name){
      this.data.comment.at = name;
      this.setData({comment : this.data.comment});
    },
    contentInput:function(e){
      this.data.comment.content = e.detail.value;
      this.setData({comment : this.data.comment});
    }
  }
})