Component({
  properties: {
    maxFileNumber: {
      type  : Number,
      value : 9
    },
    btnName: {
      type  : String,
      value : "添加照片"
    },
    items: {
      type: Array,
      value: []
    },
    closeBtnColor: {
      type: String,
      value: "#666666"
    },
	imgMode:{
		type:String,
		value:'widthFix'
	}
  },
  methods: {
    addImg: function () {
      var num = this.data.maxFileNumber - this.data.items.length;
      if (num < 1) { return false; }
      wx.chooseImage({
        count: num,
        sizeType: ['compressed'],
        success: (res) => {
          for(let i = 0; i < res.tempFilePaths.length; i++){
						this.data.items.push({url:res.tempFilePaths[i], progress:0})
          }
          this.setData({ items: this.data.items});
          this.triggerEvent('change', this.data.items);
        },
        fail: function () {}
      });
    },
    removeImg: function (e) {
      var index = e.currentTarget.id.replace('grace-items-img-', '');
      var removeImg = this.data.items.splice(index, 1)
      this.setData({ items: this.data.items});
      this.triggerEvent('change', this.data.items);
      this.triggerEvent('removeImg', removeImg[0]);
    },
    showImgs: function (e) {
      var currentImg = e.currentTarget.dataset.imgurl;
      var imgs = [];
			for(let i = 0; i < this.data.items.length; i++){
				imgs.push(this.data.items[i].url);
			}
      wx.previewImage({
        urls: imgs,
        current: currentImg
      })
    }
  }
})
