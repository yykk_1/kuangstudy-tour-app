// graceUI/components/graceReload.js
Component({
  properties: {
    refreshText     : {type:Array, value : ['继续下拉刷新','松开手指开始刷新','数据刷新中','数据已刷新']},
		refreshBgColor  : {type:Array, value : ['#FFFFFF','#FFFFFF','#FFFFFF','#63D2BC']},
		refreshColor    : {type:Array, value : ['rgba(69, 90, 100, 0.6)','rgba(69, 90, 100, 0.6)','#63D2BC','#FFFFFF']},
		refreshFontSize : {type:String, default:'26rpx'}
  },
  data: {
    reScrollTop         : 0,
    refreshHeight       : 0,
    refreshY            : 0,
    refreshStatus       : 0,
    refreshTimer        : 0
  },
  methods: {
    
		touchstart : function (e){
			this.data.reScrollTop = e.scrollTop;
			this.data.refreshY    = e.moveY;
		},
		touchmove : function(e){
			this.data.reScrollTop = e.scrollTop;
			if(this.data.refreshStatus >= 1){ return null;}
			if(this.data.reScrollTop > 10){return ;}
			if(this.data.refreshStatus != 0){return ;}
			var moveY = e.moveY - this.data.refreshY;
			moveY     = moveY / 2;
			if(moveY >= 50){
				moveY = 50;
        this.setData({refreshStatus : 1});
			}
      if(moveY > 15){this.data.refreshHeight = moveY;}
      this.setData({refreshHeight : this.data.refreshHeight});
		},
		touchend : function (e) {
			this.data.reScrollTop = e.scrollTop;
			if(this.data.refreshStatus < 1){
				return this.resetFresh();
			}else if(this.data.refreshStatus == 1){
        this.setData({refreshStatus : 2});
        this.triggerEvent('reload');
			}
		},
		scroll:function(e){
			this.data.reScrollTop = e.detail.scrollTop;
		},
		endReload : function(){
      this.setData({refreshStatus : 3});
			setTimeout(()=>{this.resetFresh()}, 800);
		},
		resetFresh : function () {
      this.setData({refreshStatus : 0, refreshHeight:0});
			return null;
		},
  }
})
