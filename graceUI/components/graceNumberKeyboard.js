// graceUI/components/graceNumberKeyboard.js
Component({
  properties: {
    show: {
      type: Boolean,
      value : false
    },
    doneBtnName: {
      type: String,
      value: "完成"
    },
    isPoint: {
      type: Boolean,
      value: true
    },
    showInputRes: { type: Boolean, value: true },
    value: { type: String, value: '' },
    confirmBtnColor : {type:String, value:'#3688FF'},
		resultColor  : {type:String, value:'#323232'},
    resultSize   : {type:String, value:'32rpx'},
    closeByShade  : {type:Boolean, default:true}
  },
  methods: {
    inputNow: function (e) {
      var k = e.currentTarget.dataset.keynumber;
      this.setData({ value: this.data.value + k + ''});
      this.triggerEvent('keyboardInput', k);
    },
    deleteNow: function () {
      this.setData({ value: this.data.value.substring(0, this.data.value.length - 1)});
      this.triggerEvent('keyboardDelete');
    },
    done: function () {
      this.triggerEvent('keyboardDone');
    },
    stopFun:function(){},
    masktap : function(){
			if(!this.data.closeByShade){return ;}
			this.done();
		}
  }
})
