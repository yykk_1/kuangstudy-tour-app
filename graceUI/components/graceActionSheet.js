// graceUI/components/graceActionSheet.js
Component({
  properties: {
		width:{type:String,value:'720rpx'},
		height:{type:String,value:'500rpx'},
		left:{type:String,value:'15rpx'},
		background:{type : String,value : 'rgba(0, 0, 0, 0.3)'},
		borderRadius : {type : String,value : '10rpx'},
		zIndex:{type : Number,value : 99},
		title:{type:String,value:''},
		titleColor:{type:String, value:'#323232'},
		items:{type:Array, value:[]},
		listColor:{type:String, value:'#3688FF'},
		listLineHeight:{type:String, value:'100rpx'},
		listFontSize:{type:String, value:'30rpx'},
		isCancelBtn:{type:Boolean, value:true},
		cancelBtnName:{type:String,value:'取消'},
		cancelBtnColor:{type:String, value:'#999999'},
		isSwitchPage:{type:Boolean, value:true}
  },
  ready:function(){
    try {
      var res = wx.getSystemInfoSync();
      res.model = res.model.replace(' ', '');
			res.model = res.model.toLowerCase();
			var res1 = res.model.indexOf('iphonex');
			if(res1 > 5){res1 = -1;}
			var res2 = res.model.indexOf('iphone1');
			if(res2 > 5){res2 = -1;}
			if(res1 != -1 || res2 != -1){
        this.setData({isIpx:true});
      }
    } catch (e){return null;}
  },
  data: {
    realShow : false,
			isIpx:false
  },
  methods: {
    cancel:function(){
      this.setData({realShow:false});
			this.triggerEvent('cancel')
		},
		closeByShade: function(){this.cancel();},
		stopFun : function(){return null;},
		selected:function (e) {
      this.triggerEvent('selected', e.currentTarget.dataset.index)
			this.close();
		},
		close:function(){this.setData({realShow:false});},
		show:function(){this.setData({realShow:true});}
  }
})
