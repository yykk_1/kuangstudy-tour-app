// graceUI/components/graceImg.js
Component({
  properties: {
    src:{type:String, value:''},
		width:{type:Number, value:300},
		height:{type:Number, value:0},
		timer : {type:Number,value:300},
		borderRadius:{type:String, value:'0rpx'}
  },
  data: {
    isLoading : true,
    imgHeight : 300,
    opacity   : 0,
    animate   : false
  },
  methods: {
    imgLoad : function (e) {
			var scale      = e.detail.width / e.detail.height;
      this.setData({imgHeight:this.data.width / scale, animate:true});
			setTimeout(() => {
        this.setData({isLoading:false, opacity:1});
      }, this.data.timer);
		}
  }
})
