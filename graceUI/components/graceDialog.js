// graceUI/components/graceDialog.js
Component({
  properties: {
    show: {
      type: Boolean,
      value: false
    },
    width: {
      type: String,
      value: '580rpx'
    },
    isCloseBtn: {
      type: Boolean,
      value: true
    },
    closeBtnColor: {
      type: String,
      value: '#FF0036'
    },
    isTitle: {
      type: Boolean,
      value: true
    },
    title: {
      type: String,
      value: ''
    },
    titleWeight: {
      type: Boolean,
      value: true
    },
    titleSize: {
      type: String,
      value: '28rpx'
    },
    titleColor: {
      type: String,
      value: '#333333'
    },
    isBtns: {
      type: Boolean,
      value: true
    },
    background: {
      type: String,
      value: 'rgba(0, 0, 0, 0.5)'
    },
    borderRadius: {
      type: String,
      value: '6rpx'
    },
    zIndex : {
      type : Number,
      value : 999
    },
    titleBg : {
      type : String,
      value : ''
    },
    titleHeight : {
      type : String,
      value : '100rpx'
    },
    canCloseByShade:{
			type : Boolean,
			value : true
		}
  },
  data: {},
  methods: {
    closeDialogByShade:function(){if(this.data.canCloseByShade){this.closeDialog();}},
    closeDialog: function () {this.triggerEvent('closeDialog');},
    stopFun: function () { },
    open:function () {this.setData({show:true});},
		hide:function () {this.setData({show:false});}
  },
  options : {
    multipleSlots : true
  }
})
