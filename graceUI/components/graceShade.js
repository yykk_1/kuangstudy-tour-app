// graceUI/components/graceShade.js
Component({
  properties: {
    background: { type: String, value : "rgba(0, 0, 0, 0.1)" },
    zIndex: { type: Number, value: 1 },
    show: { type: Boolean, value: false }
  },
  data:{
    shadeShow : false,
		showInReal : false
  },
  methods:{
    closeShade: function() {
      this.triggerEvent('closeShade');
    },
    nottmove:function(){},
    showIt : function(){
      this.setData({shadeShow:true});
			setTimeout(()=>{
        this.setData({showInReal:true});
			}, 50);
		},
		hideIt : function(){
			this.setData({showInReal:false});
			setTimeout(()=>{
				this.setData({shadeShow:false});
			}, 150);
		}
  },
  ready : function(){
    if(this.data.show){ this.showIt(); }else{ this.hideIt();}
    watch(this, {
      show: function (vn, vo) {
        if(vn){ this.showIt(); }else{ this.hideIt(); }
      }
    });
  }
});

function defineReactive(data, key, val, fn) {
  Object.defineProperty(data, key, {
    configurable: true,
    enumerable: true,
    get: function () {return val},
    set: function (newVal) {
      if (newVal === val) return
      fn && fn(newVal)
      val = newVal
    },
  })
}
function watch(ctx, obj) {
  Object.keys(obj).forEach(key => {
    defineReactive(ctx.data, key, ctx.data[key], function (value) {
      obj[key].call(ctx, value)
    })
  })
}
