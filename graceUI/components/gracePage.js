// graceUI/components/gracePage.js
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  properties: {
    customHeader: {
      type: Boolean,
      value: true
    },
    headerHeight: {
      type: Number,
      value: 44
    },
    headerIndex: {
      type: Number,
      value: 98
    },
    headerBG: {
      type: String,
      value: 'none'
    },
    statusBarBG: {
      type: String,
      value: 'none'
    },
    footerIndex: {
      type: Number,
      value: 98
    },
    rbWidth: { type: Number, value: 100 },
    rbBottom: { type: Number, value: 100 },
    rbRight: { type: Number, value: 20 },
    footerBg: { type: String, value: '' },
    isSwitchPage: { type: Boolean, value: false },
    borderWidth: { type: String, value: '0px' },
    borderColor: { type: String, value: '#D1D1D1' },
    rbIndex: { type: Number, value: 1},
    loadingBG    : { type : String,  value : 'rgba(255,255,255, 0.1)'},
		isLoading    :  { type : Boolean, value : false },
    loadingPointBg : {type : String,  value : '#3688FF'},
    footerBottom  : { type : String,  value : '0rpx' },
    bounding : {type : Boolean,  value : true}
  },
  data: {
    statusBarHeight: 44,
    iphoneXButtomHeight: 0,
    BoundingWidth: '0px',
    fixedTop      : 0
  },
  methods:{
    stopFun:function(){},
    getHeaderHeight:function(){
			return this.data.headerHeight + this.data.statusBarHeight;
		}
  },
  ready: function(){
    try {
      var system = wx.getSystemInfoSync();
      system.model = system.model.replace(' ', '');
      system.model = system.model.toLowerCase();
      var res1 = system.model.indexOf('iphonex');
      if(res1 > 5){res1 = -1;}
      var res2 = system.model.indexOf('iphone1');
      if(res2 > 5){res2 = -1;}
      if(res1 != -1 || res2 != -1){
        this.setData({ iphoneXButtomHeight : 50 * (system.windowWidth / 750)});
      }
      if(this.customHeader){
				this.fixedTop = this.data.headerHeight + system.statusBarHeight;
			}else{
				this.fixedTop = 0;
			}
      if(!this.data.customHeader){return null;}
      this.setData({ statusBarHeight : system.statusBarHeight });
      // 小程序胶囊按钮
      var Bounding = wx.getMenuButtonBoundingClientRect();
      this.setData({ BoundingWidth: (Bounding.width + system.windowWidth - Bounding.right + 10) + 'px' });
    } catch (e) {
      return null;
    }
  }
})