// graceUI/components/gracePK.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    height       : {type:String, value:'260rpx'},
		borderRadius : {type:String, value:'8rpx'},
		title        : {type:Array , value: ['','']},
		btnName      : {type:String, value:'站队'},
		status       : {type:String, value:'button'},
		progress     : {type:Array , value:[80,20,'8000 票', '2000 票']}
  },

  /**
   * 组件的方法列表
   */
  methods: { 
    choose:function(e){
      this.triggerEvent('choose', e.currentTarget.dataset.index);
    }
  }
})