// graceUI/components/graceCheckList.js
Component({
  properties: {
    height:{type:Number, value:0},
		lists : {type:Array, value:[]},
		checkColor:{type:String, value:"#3688FF"},
		imgSize:{type:Array, value:['68rpx','68rpx']},
		isBorder:{type:Boolean, value:true},
		fontSize:{type: String, value: "28rpx"},
		itemColor:{type:String, value:"#323232"},
		imgBordeRadius:{type: String, value: "8rpx"},
		smallTextColor:{type:String, value:"#999999"},
		smallTextSize:{type:String, value:"24rpx"},
		batch:{type:Boolean, value:false}
  },
  data: {
    sedAll:false,
    sedNumbers:0
  },
  methods: {
    choose:function(e){
			if(!this.data.batch){return ;}
			var index = e.currentTarget.dataset.index;
			if(this.data.lists[index].checked){
				this.data.lists[index].checked = false;
				this.data.lists.splice(index, 1, this.data.lists[index]);
			}else{
				this.data.lists[index].checked = true;
				this.data.lists.splice(index, 1, this.data.lists[index]);
			}
      this.setData({lists:this.data.lists});
			var sedArr = [], sedNumbers = 0;
			for(let i = 0; i < this.data.lists.length; i++){
        if(this.data.lists[i].checked){
          sedArr.push(i);
          sedNumbers++;
        }
      }
      this.setData({sedNumbers:sedNumbers});
      if(sedNumbers >= this.data.lists.length){
        this.setData({sedAll:true});
      }else{
        this.setData({sedAll:false});
      }
			this.triggerEvent('change', sedArr);
		},
		selectAll : function(){
			if(this.data.sedAll){
				for(let i = 0; i < this.data.lists.length; i++){
					this.data.lists[i].checked = false;
					this.data.lists.splice(i, 1, this.data.lists[i]);
        }
        this.setData({lists:this.data.lists, sedAll:false});
				this.triggerEvent('change', []);
			}else{
				var sedArr = [];
				for(let i = 0; i < this.data.lists.length; i++){
					this.data.lists[i].checked = true;
					this.data.lists.splice(i, 1, this.data.lists[i]);
					sedArr.push(i);
				}
				this.setData({lists:this.data.lists, sedAll:true});
				this.triggerEvent('change', sedArr);
			}			
		}
  }
})
