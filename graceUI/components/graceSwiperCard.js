Component({
  properties: {
    width: {
      type: String,
      value: "100%"
    },
    height: {
      type: String,
      value: "300rpx"
    },
    swiperItems: {
      type: Array,
      value: []
    },
    borderRadius: {
      type: String,
      value: '5rpx'
    },
    indicatorColor: {
      type: String,
      value: "rgba(0, 0, 0, 0.2)"
    },
    indicatorActiveColor: {
      type: String,
      value: "#3688FF"
    },
    spacing: {
      type: String,
      value: "50rpx"
    },
    interval: {
      type: Number,
      value: 5000
    },
    padding: {
      type: String,
      value: '0 10rpx'
    },
    scale: {
      type: Boolean,
      value: true
    },
    autoplay : {
      type : Boolean,
      value : true
    },
    currentIndex : {
			type : Number,
			value : 0
    },
    indicatorDots:{
			type : Boolean,
			value:true
		}
  },
  data: {
    isReady: false
  },
  methods: {
    swiperchange: function (e) {
      this.setData({ currentIndex: e.detail.current, isReady:true});
      this.triggerEvent('swiperchange', e.detail.current);
    }
  }
})
