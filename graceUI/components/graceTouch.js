// graceUI/components/graceTouch.js
Component({
  properties: {
		datas:{type:Array, value:[]}
  },
  data: {
    toucheTimer  : 0,
		fingerRes    : [],
		distance     : 0,
		isTap        : true
  },
  methods: {
    toInt : function(arr){
			var res = [];
			arr.forEach((item)=>{
				item.pageX = parseInt(item.pageX);
				item.pageY = parseInt(item.pageY);
				res.push(item);
			});
			return res;
		},
		touchstart : function(e){
			this.data.isTap        = true;
			this.data.fingerRes    = this.toInt(e.touches);
			if(this.data.fingerRes.length > 2){return ;}
			this.data.toucheTimer  = new Date().getTime();
			var moves = [], i = 0;
			this.data.fingerRes.forEach((finger)=>{
				var xTouch = finger.pageX;
				var yTouch = finger.pageY;
				moves.push([xTouch, yTouch]);
				i++;
			});
			this.triggerEvent('thStart', [moves, this.data.datas]);
		},
		touchmove : function(e){
			setTimeout(()=>{this.data.isTap = false;}, 100);
			var touches = this.toInt(e.touches);
			if(touches.length > 2){return ;}
			if(touches.length == 1){
				var i = 0, moves = [];
				touches.forEach((finger)=>{
					var xTouch = finger.pageX - this.data.fingerRes[i].pageX;
					var yTouch = finger.pageY - this.data.fingerRes[i].pageY;
					moves.push([xTouch, yTouch]);
					i++;
				});
				this.triggerEvent('thMove', [moves, this.data.datas]);
			}
			else if(touches.length == 2){
				if(this.data.distance == 0){
					this.data.distance = parseInt(this.getDistance(touches[0].pageX,touches[0].pageY, touches[1].pageX, touches[1].pageY));
				}else{
					var distance1 = parseInt(this.getDistance(touches[0].pageX,touches[0].pageY, touches[1].pageX, touches[1].pageY));
					var scale = distance1 / this.data.distance;
					scale = Math.floor(scale * 100) / 100;
					this.triggerEvent('scale', [scale, this.data.datas]);
				}
			}
		},
		touchend : function (e){
			if(this.data.isTap){this.triggerEvent('tapme'); return ;}
			var touches   = this.toInt(e.changedTouches);
			this.data.distance = 0;
			if(touches.length == 1){
				var i = 0, moves = [];
				touches.forEach((finger)=>{
					var xTouch = finger.pageX - this.data.fingerRes[i].pageX;
					var yTouch = finger.pageY - this.data.fingerRes[i].pageY;
					moves.push([xTouch, yTouch]);
					i++;
				});
				var timer = new Date().getTime() - this.data.toucheTimer;
				moves.push(timer);
				this.triggerEvent('thEnd', [moves, this.data.datas]);
				// 根据时间及距离决定滑动时间
				if(timer < 300){
					var mx = Math.abs(moves[0][0]);
					var my = Math.abs(moves[0][1]);
					if(mx > my){
						if(mx >= 50){
							if(moves[0][0] > 0){
								this.triggerEvent('swipe', ['right', this.data.datas]);
							}else{
								this.triggerEvent('swipe', ['left', this.data.datas]);
							}
						}
					}else{
						if(my >= 50){
							if(moves[0][1] > 0){
								this.triggerEvent('swipe', ['down', this.data.datas]);
							}else{
								this.triggerEvent('swipe', ['up', this.data.datas]);
							}
						}
					}
				}
			}
		},
		getDistance : function (lat1,  lng1,  lat2,  lng2){
			var radLat1  = lat1*Math.PI / 180.0;
			var radLat2  = lat2*Math.PI / 180.0;
			var a        = radLat1 - radLat2;
			var b        = lng1*Math.PI / 180.0 - lng2*Math.PI / 180.0;
			var s        = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a/2),2) + Math.cos(radLat1)*Math.cos(radLat2)*Math.pow(Math.sin(b/2),2)));
			s            = s * 6378.137;
			return Math.round(s * 10000) / 10000;
		},
		tapmd : function(){
			this.data.isTap = false;
		}
  }
})
