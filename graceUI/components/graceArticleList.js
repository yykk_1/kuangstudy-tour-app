// graceUI/components/graceArticleList.js
Component({
  properties: {
    articles   : {type:Array, value:[]},
		titleStyle : {type:Object, value: {'lineHeight':'50rpx', 'fontSize':'36rpx', 'color':'#323232'}}
  },
  methods: {
    newstap : function(e){
			this.triggerEvent('newstap', e.currentTarget.dataset.articleid);
		}
  }
})