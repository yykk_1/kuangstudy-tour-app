// graceUI/components/graceDate.js
import guiCalendar from './graceCalendar.js';
Component({
  properties: {
    show: {
      type: Boolean,
      value: false
    },
    currentDate: {
      type: String,
      value: ""
    },
    isTime: {
      type: Boolean,
      value: true
    },
    top: {
      type: String,
      value: '0'
    },
    zIndex: {
      type: String,
      value: "1"
    },
    bgColor: { type: String, value: "#F6F7F8" },
    activeBgColor: { type: String, value: "#3688FF" },
    borderRadius: { type: String, value: "6rpx" },
    isLunar: { type: Boolean, value: true }
  },
  data: {
    weeks: ['一', '二', '三', '四', '五', '六', '日'],
    cYear: 2016,
    cMonth: 6,
    cMonthStr: "06",
    cDay: "01",
    days: '',
    currentDayIn: '',
    currentTimeIn: ''
  },
  methods: {
    close : function(){
      this.triggerEvent("closeDate");
    },
    timechange: function (e) {
      this.setData({ currentTimeIn: e.detail.value});
    },
    getDaysInOneMonth : function (){
      var d = new Date(this.data.cYear, this.data.cMonth, 0);
      return d.getDate();
		},
		getDay: function (){
      var d = new Date(this.data.cYear, this.data.cMonth - 1, 0);
      return d.getDay();
		},
    prevYear: function () {
      this.setData({cYear : this.data.cYear - 1});
      this.changeMonth();
    },
		prevMonth : function(){
      this.data.cMonth = this.data.cMonth - 1;
      if (this.data.cMonth < 1) { this.data.cMonth = 12; this.data.cYear = this.data.cYear - 1; }
      this.data.cMonthStr = this.data.cMonth < 10 ? '0' + this.data.cMonth : this.data.cMonth;
      this.setData({
        cMonth : this.data.cMonth,
        cMonthStr : this.data.cMonthStr,
        cYear : this.data.cYear
      });
      this.changeMonth();
		},
		nextMonth : function(){
      this.data.cMonth = this.data.cMonth + 1;
      if (this.data.cMonth > 12) { this.data.cMonth = 1; this.data.cYear = this.data.cYear + 1; }
      this.data.cMonthStr = this.data.cMonth < 10 ? '0' + this.data.cMonth : this.data.cMonth;
      this.setData({
        cMonth: this.data.cMonth,
        cMonthStr: this.data.cMonthStr,
        cYear: this.data.cYear
      });
      this.changeMonth();
		},
    nextYear: function () {
      this.setData({ cYear: this.data.cYear + 1 });
      this.changeMonth();
    },
		changeMonth:function(y, m){
      var daysList  = [];
      var days      = this.getDaysInOneMonth();
      var startWeek = this.getDay();
      var forSteps  = 0;
      for (var i = (0 - startWeek); i < days; i++) {
        if (i >= 0) {
          daysList[forSteps] = { date: i >= 9 ? i + 1 : '0' + (i + 1), nl: '' };
          daysList[forSteps].nl =  guiCalendar.getLunarInfo(this.data.cYear, this.data.cMonth, i + 1);
        } else {
          daysList[forSteps] = '';
        }
        forSteps++;
      }
			this.setData({ days: daysList});
		},
		chooseDate: function (e) {
      var sedDate = e.currentTarget.dataset.cdate;
			var arr = sedDate.split('-');
      if (arr[2] == 'undefined'){ return; }
      this.setData({ currentDayIn:sedDate});
      if(this.data.isTime){return ;}
      this.triggerEvent('changeDate', sedDate);
		},
    submit: function () {
      if (this.data.isTime) {
        this.triggerEvent('changeDate', this.data.currentDayIn + ' ' + this.data.currentTimeIn);
      } else {
        this.triggerEvent('changeDate', this.data.currentDayIn);
      }
    },
    //初始化时间
    initTime: function () {
      if (this.data.currentDate == '') {
        var dateObj             = new Date();
        this.data.cYear         = dateObj.getFullYear();
        this.data.cMonth        = dateObj.getMonth() + 1;
        this.data.cMonthStr     = this.data.cMonth < 10 ? '0' + this.data.cMonth : this.data.cMonth;
        this.data.cDay = dateObj.getDate();
        this.data.cDay = this.data.cDay < 10 ? '0' + this.data.cDay : this.data.cDay;
        this.data.currentDayIn = this.data.cYear + '-' + this.data.cMonthStr + '-' + this.data.cDay;
        this.data.currentTimeIn = '00:00';
      } else {
        var dates               = this.data.currentDate.split(' ');
        if (!dates[1]) { dates[1] = '';}
        var dayArr              = dates[0].split('-');
        this.data.cYear         = Number(dayArr[0]);
        this.data.cMonth        = dayArr[1];
        this.data.cDay          = dayArr[2];
        var reg = new RegExp('^0[0-9]+$');
        if (reg.test(this.data.cMonth)) {this.data.cMonth = this.data.cMonth.substr(1, 1); }
        this.data.cMonth        = Number(this.data.cMonth);
        this.data.cMonthStr     = this.data.cMonth < 10 ? '0' + this.data.cMonth : this.data.cMonth;
        this.data.currentDayIn  = dates[0];
        this.data.currentTimeIn = dates[1];
      }
      this.setData({
        cYear : this.data.cYear,
        cMonth: this.data.cMonth,
        cMonthStr: this.data.cMonthStr,
        cDay: this.data.cDay,
        currentDayIn: this.data.currentDayIn,
        currentTimeIn: this.data.currentTimeIn
      });
      this.changeMonth();
    },
    run : function(){this.initTime();},
    stopFun: function(){},
    open : function(){this.setData({show:true});},
    hide : function(){this.setData({show:false});}
  }
});