// graceUI/components/graceSwipeList.js
Component({
  properties: {
    width:{type:Number, value:750},
		msgs:{type:Array,value:[]},
		imgSize:{type:Array,value:['80rpx', '80rpx']},
		fontSizes:{type:Array,value:['28rpx','22rpx', '22rpx']},
		fontColors:{type:Array,value:['#323232','#888888', '#888888']},
    btnWidth:{type:Number, value:160},
    height:{type:Number, default:200}
  },
  data: {
    msgsIn    : [],
    damping   : 0.1,
    moveIndex : -1,
		x         : 0,
    oX        : 0,
    scY       : true
  },
  ready:function(){
    this.init(this.data.msgs);
    watch(this, {
      msgs: function (vn, vo) {
			  this.init(vn);
      }
    });
  },
  methods: {
		init:function(msgs){
      this.setData({moveIndex : -1, msgsIn : msgs});
		},
		thStart : function(e){
      var index = e.detail[1][0];
			this.setData({damping : 0.1, oX:this.data.x, x:0, moveIndex:index});
		},
		thMove : function (e){
      e = e.detail;
      var x = Math.abs(e[0][0][0]);
			var y = Math.abs(e[0][0][1]);
			if(y > x){
        this.setData({scY : true, moveIndex : -1});
        return ;
      }
      this.setData({scY : false});
      this.data.x -= e[0][0][0] * this.data.damping;
			if(this.data.x > this.data.btnWidth){this.data.x = this.data.btnWidth;}
			if(this.data.x < 0){this.data.x = 0;}
      this.data.damping *= 1.02;
      this.setData({x:this.data.x, damping:this.data.damping});
		},
		thEnd : function(e){
      if(this.data.x < this.data.btnWidth / 3){
        this.data.x = 0;
      }else{
        this.data.x = this.data.btnWidth;
      }
      this.setData({x:this.data.x});
      this.setData({scY : true});
		},
		btnTap : function (e) {
      var datas = e.currentTarget.dataset.indexs;
      this.triggerEvent('btnTap', datas);
    },
    itemTap : function(e){
      this.triggerEvent('itemTap', e.currentTarget.dataset.index);
    },
    tapin : function () {
			if(this.data.oX < 30){
        this.triggerEvent('itemTap',this.data.moveIndex);
        this.setData({scY : true, moveIndex : -1});
      }
		}
	}
})
function defineReactive(data, key, val, fn) {
  Object.defineProperty(data, key, {
    configurable: true,
    enumerable: true,
    get: function () {return val;},
    set: function (newVal) {
      if (newVal === val) return
      fn && fn(newVal)
      val = newVal
    }
  })
}
function watch(ctx, obj) {
  Object.keys(obj).forEach(key => {
    defineReactive(ctx.data, key, ctx.data[key], function (value) {obj[key].call(ctx, value);});
  });
}