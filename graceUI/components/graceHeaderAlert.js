// graceUI/components/graceAlert.js
Component({
  properties: {
		show:{
			type : Boolean,
			value : false
		},
    background : {
      type : String,
      value: '#F1F2F3'
    },
    top: {
      type: Number,
      value: 20
    },
    width      : { type : Number, value : 580},
    duration   : { type : Number, value : 2500}
  },
  options: {
    multipleSlots: true
  },
  methods:{
		open:function(){
      this.setData({show:true});
			setTimeout(()=>{this.setData({show:false});}, this.data.duration);
		},
		close:function(){
			this.setData({show:false})
		}
	}
})
