
module.exports = {
  info: function () {
    try {
      var res = wx.getSystemInfoSync();
      var iPhoneXBottom = 0;
      res.rpx2px = res.windowWidth / 750;
      res.model = res.model.replace(' ', '');
      if (res.model.indexOf('iPhoneX') != -1 || res.model.indexOf('iPhone X') != -1) {
        res.iphonexbottomheightrpx = 50;
        res.iphonexbottomheightpx  = 50 * res.rpx2px;
      } else {
        res.iphonexbottomheightrpx = 0;
        res.iphonexbottomheightpx = 0;
      }
      return res;
    } catch (e) {
      return null;
    }
  }
}