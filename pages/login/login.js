var app = getApp();
var auth = require("../../utils/auth");

Page({
  data: {
    canIUse: auth.canIUse
  },

  onLoad(){
    auth.weixinLogin();
  },

  //如果没有，必须点击一个按钮，然后触发自定义的方法bindGetUserInfo,参数e就是你授权确定以后的用户的信息数据。
  getUserInfo (e) {
    auth.loginWithWx(e.detail.userInfo);
  }

})