Page({
  data: {
    currentIndex: 0,
    mainHeight: 600,
    //分类数据
    tabsAll: [],
    tabs: [],
    // 新闻信息保存数组
    newsAll: [],
    // 每个选项卡对应的分页
    pages: [],
    // 加载状态
    loadingTypes: [],
    // 每个滚动区域的滚动值
    scrollTops: []
  },
  onReady: function () {
    setTimeout(() => {
      wx.createSelectorQuery().select('#gBody').fields(
        { size: true }, (res) => {
          this.setData({ mainHeight: res.height });
        }
      ).exec();
    }, 1000);
    //加载分类信息 初始化数据
    this.getCate();
  },
  navChange: function (e) {
    this.setData({ currentIndex: e.detail });
  },
  swiperChange: function (e) {
    var index = e.detail.current;
    this.setData({ currentIndex: index });
    if (this.data.loadingTypes[this.data.currentIndex] != 2 || this.data.loadingTypes[this.ata.currentIndex] != 4) { this.getNews(); }
  },
  //加载分类信息 初始化数据
  getCate: function () {
    wx.request({
      // 此处可以获取分类 根据分类自己拼接请求地址
      // 格式请参考 接口 [ 浏览器运行直接查看 ]
      url: 'http://grace.hcoder.net/api/tabs',
      method: 'GET',
      data: {},
      success: res => {
        // 初始化新闻列表数组 元素数量与分类匹配
        this.data.tabsAll = res.data.data;
        for (var i = 0; i < this.data.tabsAll.length; i++) {
          this.data.newsAll.push([]);
          this.data.tabs.push(this.data.tabsAll[i].txt);
          this.data.pages.push(1);
          this.data.loadingTypes.push(3);
          this.data.scrollTops.push(0);
        }
        this.setData({
          newsAll: this.data.newsAll,
          tabs: this.data.tabs,
          pages: this.data.pages,
          loadingTypes: this.data.loadingTypes,
          scrollTops: this.data.scrollTops
        });
        this.getNews();
      }
    });
  },
  // 加载新闻
  getNews: function (isReload) {
    // 当前正在展示的 选项index 为 this.data.currentIndex
    // 那么分类 id 应该为 this.data.tabsAll[this.data.currentIndex].id
    //console.log('类型 : ' + this.tabs[this.data.currentIndex] + ' 第'+ this.data.pages[this.data.currentIndex] +'页');
    if (!isReload) {
      this.data.loadingTypes.splice(this.data.currentIndex, 1, 1);
      this.setData({ loadingTypes: this.data.loadingTypes });
    }
    //console.log('http://grace.hcoder.net/api/news/index/'+this.data.tabsAll[this.data.currentIndex].id+'/'+this.data.pages[this.data.currentIndex]);
    wx.request({
      // 此处可以获取分类 根据分类自己拼接请求地址
      // 分类 id 、页码 都已经获取到了
      url: 'http://grace.hcoder.net/api/news/index/' + this.data.tabsAll[this.data.currentIndex].id + '/' + this.data.pages[this.data.currentIndex],
      method: 'GET',
      data: {},
      success: res => {
        if (res.data.status == 'ok') {
          // 第一页
          if (this.data.pages[this.data.currentIndex] == 1) {
            this.data.newsAll.splice(this.data.currentIndex, 1, res.data.data);
            this.setData({ newsAll: this.data.newsAll });
          }
          // 之后的加载页
          else {
            this.data.newsAll[this.data.currentIndex] = this.data.newsAll[this.data.currentIndex].concat(res.data.data);
            this.setData({ newsAll: this.data.newsAll });
          }
          // 页码增加
          this.data.pages[this.data.currentIndex]++;
          this.setData({ pages: this.data.pages });
          setTimeout(() => {
            this.data.loadingTypes.splice(this.data.currentIndex, 1, 3);
            this.setData({ loadingTypes: this.data.loadingTypes });
          }, 300)
        } else if (res.data.status == 'empty') {
          console.log('empty');
          this.data.newsAll[this.data.currentIndex] = 'empty';
          this.data.loadingTypes.splice(this.data.currentIndex, 1, 4);
          this.setData({ newsAll: this.data.newsAll, loadingTypes: this.data.loadingTypes });
        } else if (res.data.status == 'nomore') {
          console.log('nomore');
          this.data.loadingTypes.splice(this.data.currentIndex, 1, 2);
          this.setData({ loadingTypes: this.data.loadingTypes });
        }
      },
      complete: () => {
        if (isReload) {
          setTimeout(() => {
            this.selectComponent("#graceReload" + this.data.currentIndex).endReload();
          }, 300)
        }
      }
    });
  },
  // 加载更多
  scrollend: function (e) {
    // 判断加载状态避免多次滚动时有加载尚未完成
    if (this.data.loadingTypes[this.data.currentIndex] == 2 || this.data.loadingTypes[this.data.currentIndex] == 4) { return false; }
    console.log('loadmore.....');
    this.getNews();
  },
  scroll: function (e) {
    this.data.scrollTops[this.data.currentIndex] = e.detail.scrollTop;
    this.setData({ scrollTops: this.data.scrollTops });
  },
  touchstart: function (e) {
    var touchObj = { scrollTop: this.data.scrollTops[this.data.currentIndex], moveY: e.changedTouches[0].pageY };
    this.selectComponent("#graceReload" + this.data.currentIndex).touchstart(touchObj);
  },
  touchmove: function (e) {
    var touchObj = { scrollTop: this.data.scrollTops[this.data.currentIndex], moveY: e.changedTouches[0].pageY };
    this.selectComponent("#graceReload" + this.data.currentIndex).touchmove(touchObj);
  },
  touchend: function (e) {
    var touchObj = { scrollTop: this.data.scrollTops[this.data.currentIndex], moveY: e.changedTouches[0].pageY };
    this.selectComponent("#graceReload" + this.data.currentIndex).touchend(touchObj);
  },
  // 下拉刷新
  reload: function () {
    this.data.pages[this.data.currentIndex] = 1;
    this.data.loadingTypes.splice(this.data.currentIndex, 1, 3);
    this.setData({
      pages: this.data.pages,
      loadingTypes: this.data.loadingTypes
    });
    this.getNews(1);
  },
  newsinfo:function(){
    wx.navigateTo({
      url:"../artInfo/artInfo"
    })
  }
})