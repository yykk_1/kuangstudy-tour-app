Page({
  data: {
    graceSkeleton: true,
    //文章对象格式
    article:{
    	title : "标题",
    	authorFace : "作者头像",
    	authorName : "作者姓名",
    	viewNumber : "浏览次数",
    	date       : "日期",
    	contents   : [
    		{type : "text", content : "文本内容"},
    		{type : "img", content : "图片路径"},
    	]
    }
  },
  onLoad: function () {
      var news = {contents:[]};
      news.title = "标题";
      wx.setNavigationBarTitle({ title: news.title });
      // 此处先规划骨架
      var art = { contents: [] };
      for (var i = 0; i < news.contents.length; i++) {
        art.contents.push({ 'type': news.contents[i].type });
      }
      this.setData({ article : art});
      // 骨架屏规划后延长 500 毫秒进行数据替换
      setTimeout(() => {
          this.setData({ article: news, graceSkeleton:false });
      }, 5000);
      
  },


  prevImg: function (e) {
    var imgs = [];
    var currentUrl = e.currentTarget.dataset.imgurl;
    for (let i = 0; i < this.data.article.contents.length; i++) {
      if (this.data.article.contents[i].type == 'img') {
        imgs.push(this.data.article.contents[i].content);
      }
    }
    
    wx.previewImage({
      urls: imgs,
      current: currentUrl
    })
  }
})