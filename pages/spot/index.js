import productService from '../../service/product/index';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    productId: "",
    // 轮播图 
    swiperItems: [],
    // 切换导航
    navItems: ['行程概要', '行程详情', '须知'],
    active: 0,
    product: {
      name: "",
      logo: "",
      price: "",
      priceMarket: "",
      viewcount:0,
      favcount:0,
      updateTime:"",
      address:"",
      latitude:"",
      lontitude:"",
      tags:"",
      img:"",
      categoryCid:"",
      categoryPid:"",
      productId:"",
      imgs: []
    },
   
  },
  // 分享
  share: function () {
    wx.showToast({
      title: '请自行完善分享代码',
      icon: "none"
    });
  },
  // 评论图片展示
  showImgs: function (e) {
    console.log(e);
    var commentsIndex = e.currentTarget.dataset.index1;
    var imgIndex = e.currentTarget.dataset.index2;
    wx.previewImage({
      urls: this.data.commentContents[commentsIndex].imgs,
      current: this.data.commentContents[commentsIndex].imgs[imgIndex]
    })
  },
 
  /* 根据产品id查询产品明细*/
  getProduct(productId) {
    productService.getProduct(productId).then(res => {
      this.changeProductData(res.data);
    });
  },

  // 数据转换
  changeProductData(product) {
    var cproduct = {
      ...product,
      name: product.productTitle,
      logo: "../../static/logo.png",
      price: product.productPrice,
      priceMarket: product.price,
      img:product.productImg,
      productdesc: product.productDesc,
      tags:product.tags?product.tags.split(","):[],
      imgs: product.thumnimgs?product.thumnimgs.split(","):[]
    };
    var swiperItems = [product.productImg,...cproduct.imgs];
    this.setData({
      swiperItems,
      product:cproduct
    });
  },

  /*支付拉起 */
  weixinPay(){
    // 1: 获取支付的产品
    var productId = this.data.productId;
    console.log("你支付的产品是：" , productId);

      // 2: 拉起微信小程序的支付功能
      wx.requestPayment({
      "timeStamp": "1630511104",
      "package": "prepay_id=wx012345034610466db626f089bc66300000",
      "paySign": "lrRkrSewY+nbrNNqVZy01oBbYE+ybqy1uW/5iW4ydwwQ8m/3vJoQAFiOfFOZD9WEJUYdgZIZNNDYTTZUG2hqAt4PFcgRpZCRSaoc6NafX7ECEfyt2FUXxH+UmaUCqZAsYZp0uRKJt84LVlnilNoSNTVKNjKkHORK3aSbB2OqnVZN4zNpMenhGFaHRvWm4D5xI+8qygtcNIjtXY+JcLLz1DltzLrSsc13wpeR1F43ss2GUtWMiau+LUt400h+AFuWtMehMw8uVrvSYM98msYv04Xx3gGobulKDHGKitm57Ia756HVn6eXWmX5bagE+JGbfJqFByWK5edswlJy9TPRTg==",
      "signType": "RSA",
      "nonceStr": "7dab2285141d4c929a076f3b3fa29dc2",
        success (res) { console.log("支付成功!!")},
        fail (res) { }
      })
  

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 保存支付产品的id信息
    this.data.productId = options.pid;
    // 调用查询产品的明细接口
    this.getProduct(options.pid);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var {name:title,img:imageUrl} = this.data.product;
    return { title,imageUrl}
  }
})