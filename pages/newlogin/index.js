
var auth = require("../../utils/auth");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    backto:"1",
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },


  onLoad(options) {
    // auth.weixinLogin(res=>{
    //   console.log(res.rawData)
    // });

    this.setData({backto:options.flag})

  },


  
  // 点击一键登陆,授权用户信息
  getUserInfo(e){
    auth.loginWeixin(e.detail.userInfo,this.data.backto);
  }

})