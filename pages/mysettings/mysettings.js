var graceChecker = require("../../graceUI/jsTools/graceChecker.js");

Page({
  data: {
    name2: '',
    pwd: '',
    pwdType: 'password',
    pwd2: '',
    pwd2Type: 'password',
    gender: ['请选择', '男', '女'],
    genderIndex: 0,
  },
  clearInput: function () {
    this.setData({name2 : ''});
  },
  pickerChange: function (e) {
    this.setData({ genderIndex: e.detail.value });
  },
  pwdinput : function(e){
    this.setData({ pwd: e.detail.value });
  },
  showPwd: function () {
    this.setData({ pwdType: this.data.pwdType == "password" ? 'text' : 'password' });
  },
  pwd2input: function (e) {
    this.setData({ pwd2: e.detail.value });
  },
  showPwd2: function () {
    this.setData({ pwd2Type: this.data.pwd2Type == "password" ? 'text' : 'password' });
  },
  pickerChange: function (e) {
    this.setData({ genderIndex: e.detail.value });
  },
  // 表单提交及验证
  formSubmit: function (e) {
    //定义表单规则
    var rule = [
      { name: "nickname", checkType: "string", checkRule: "1,3", errorMsg: "姓名应为1-3个字符" },
      { name: "pwd", checkType: "string", checkRule: "6,", errorMsg: "密码至少6个字符" },
      { name: "pwd2", checkType: "samewith", checkRule: "pwd", errorMsg: "两次密码输入不一致" },
      { name: "gender", checkType: "in", checkRule: "1,2", errorMsg: "请选择性别" },
      { name: "aihao", checkType: "notnull", checkRule: "", errorMsg: "请选择爱好" },
      { name: "desc", checkType: "string", checkRule: "5,", errorMsg: "自我介绍至少5个字符" }
    ];
    //进行表单检查 e.detail.value 内存放着表单数据
    var formData = e.detail.value;
    var checkRes = graceChecker.check(formData, rule);
    if (checkRes) {
      wx.showToast({ title: "验证通过!", icon: "none" });
    } else {
      wx.showToast({ title: graceChecker.error, icon: "none" });
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})