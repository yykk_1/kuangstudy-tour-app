// 1: 导入service
import productService from '../../service/product/index'

Page({
  data: {

    // 查询二级分类
    categoryCid:"",
    // 查询一级分类
    categoryPid:"",
    // 当前页
    pageNo:1,
    // 每页显示多少个产品
    pageSize:10,
    // 装载产品的数组容器
    productList: [[],[]],
    // 这个底部加载进度控制显示 0 初始化中 1 上拉加载更多 2 加载已完成 3 不显示
    loadingType: 3,
    loadingText:["赶紧的我下面很多数据哦...", "我在霸蛮的加载中...", "亲，我也是有底线的哦", '数据正在来的路上...', ''],
    // 总页数
    pages:0,
    // 总记录数
    total:0,

    // 第1个菜单
    selectVal1: 0,
    show1: false,
    selectMenu1: ['景点', '自然风光', '名胜古迹'],
    categoryList1: [{id:1,name:"景点",isroot:true}, {id:2,name:"自然风光",isroot:false}, {id:3,name:"名胜古迹",isroot:false}],
    // 第2个菜单
    selectVal2: 0,
    show2: false,
    selectMenu2: ['离我最近', '100 - 1000', '1000 - 10000', '10000 +'],
    // 第3个菜单
    selectVal3: 0,
    show3: false,
    selectMenu3: ['商圈', '黑色', '蓝色', '红色'],
    
    // 侧边抽屉
    filterHeight: 300,
    scrollHeight: 300,
    showFilter: false,
    filterVal1: 0, // 用于保存选中值、参与表单提交
    filterItems1: [{
        name: '条件1',
        value: '0',
        checked: true
      },
      {
        name: '条件2',
        value: '1',
        checked: false
      },
      {
        name: '条件3',
        value: '2',
        checked: false
      },
      {
        name: '条件4',
        value: '3',
        checked: false
      },
      {
        name: '条件5',
        value: '4',
        checked: false
      },
      {
        name: '条件6',
        value: '5',
        checked: false
      }
    ]
  },

  onLoad: function (options) {
    
  },

  onShow(){
    var cpid = wx.getStorageSync('cpid');
    this.setData({pageNo:1,pageSize:10,categoryPid:cpid,loadingType:0,pages:0,total:0,categoryCid:"",productList:[[],[]]})
    // 查询产品数据
    this.getList();
  },


  // 查询产品信息
  getList: function (callback) {
    // 控制查询分页的边界
    var {pageNo,pages,pageSize,categoryCid,categoryPid} = this.data;
    // pages > 0 代表的含义：1：还没有初始化 2：代表getList是第一次调用
    if(pages > 0 && pageNo > pages ){
      // 这里是下拉加载分页的开关
      this.setData({loadingType:2});
      wx.showToast({
        title: '已经加载完毕了....',
      })
      return;
    }
    
    // 查询产品API
    productService.loadProduct({pageNo,pageSize,categoryCid,categoryPid}).then(res => {

      // 1: 准备两个左右的数组容器
      var lArr = this.data.productList[0];
      var rArr = this.data.productList[1];

      // 2: 遍历所有的数据
      var {pages,total,records:products} = res.data;
      products.map(res=>{
        res.tags = res.tags?res.tags.split(","):[];
        return res; 
      }); 

      for (var i = 0; i < products.length; i++) {
        if (i % 2 == 0) {
          lArr.push(products[i]);
        } else {
          rArr.push(products[i]);
        }
      }

      // 3: 放数据
      var ltype = this.getloadingType(total);
      this.setData({
        productList: [lArr, rArr],
        pageNo:++pageNo,
        pages:pages,
        total:total,
        loadingType:ltype
      });

      callback && callback();
    })
  },

  getloadingType(total){
    // 0: 上拉加载更多", 1：:正在努力加载", 2：已经加载全部数据", 3：''
    var ltype = 0;
    // 1: 如果你当前产品数量 24 10
    if(total == 0){
      ltype = 3;
    }else if( this.data.pageSize >= total){
      ltype = 2;
    }
    return ltype;
  },

  
  //上拉加载更多
  onReachBottom: function () {
    // 这里有一个边界判断
    if(this.data.loadingType == 2){
        return;
    }

    this.setData({loadingType:1});
    this.getList();
  },


  // 下拉刷新
  onPullDownRefresh(){
    // 清空数据
    this.setData({pageNo:1,pageSize:10,loadingType:0,pages:0,total:0,categoryCid:"",productList:[[],[]]})
    // 在查询所有产品信息
    this.getList(function(){
        // 4:下拉刷新让下拉动画立即结束
        wx.stopPullDownRefresh()
    });
  },




  // 下拉选择
  showMenu1: function () {
    this.setData({
      show1: true
    });
  },

  closeMenu1: function () {
    this.setData({
      show1: false
    });
  },


  select1: function (selectItem) {
    // 获取点击分类索引信息
    var selectIndex = selectItem.detail;
    // 获取点击分类索引信息查询对应的分类
    var category = this.data.categoryList1[selectIndex];
    // 根据分类判断是一级分类isroot=true 还是二级分类isroot=false
    if(category.isroot){
        this.setData({categoryPid:category.id});
    }else{
        this.setData({categoryCid:category.id});
    }

    // 检索记得清空一次
    this.setData({productList:[],pageNo:1,pageSize:10});
    this.getList();
  },



  showMenu2: function () {
    this.setData({
      show2: true
    });
  },
  closeMenu2: function () {
    this.setData({
      show2: false
    });
  },
  select2: function (index) {
    index = index.detail;
    console.log("选择了 " + this.data.selectMenu2[index]);
  },
  showMenu3: function () {
    this.setData({
      show3: true
    });
  },
  closeMenu3: function () {
    this.setData({
      show3: false
    });
  },
  select3: function (index) {
    index = index.detail;
    console.log("选择了 " + this.data.selectMenu3[index]);
  },
  showMenu4: function () {
    this.setData({
      show4: true
    });
  },
  closeMenu4: function () {
    this.setData({
      show4: false
    });
  },
  select4: function (index) {
    index = index.detail;
    console.log("选择了 " + this.data.selectMenu4[index]);
  },
  // 条件筛选
  openFilter: function () {
    this.setData({
      showFilter: true
    });
  },
  closeFilter: function () {
    this.setData({
      showFilter: false
    });
  },
  // 可选标签事件
  filterChange1: function (val) {
    this.setData({
      filterVal1: val
    });
  },
  filterSubmit: function () {
    // 收集相关数据,如:
    console.log(this.data.filterVal1);
    // 关闭筛选
    this.setData({
      showFilter: false
    });
  },
  filterReset: function () {
    // 将筛选的值恢复初始即可，如:
    //this.setData({*** : ***});
    this.selectComponent('#graceSelectTags1').graceSelectChange(0);
  }




})