var auth = require("../../utils/auth");

Page({
  isLogin:false,
  user:{},
  data: {
    items: [
      [80, '', '我的订单'],
      [100, '', '待支付'],
      [50, '', '待出行'],
      ['￥199', '', '已出行']
    ]
  },

  // 用户状态监听
  onShow(){
    console.log(auth.isLogin())
    console.log(auth.getUser())
    this.setData({
      isLogin:auth.isLogin(),
      user:auth.getUser()
    })
  },

  goBuy(e){
     var index = e.detail;
     wx.navigateTo({
       url: '../myorders/myorders?oindex='+index
     })
  },
  
  callphone(){
    wx.makePhoneCall({
      phoneNumber: '15074816437' //仅为示例，并非真实的电话号码
    })
  },

  // 用户退出
  logout(){
    auth.logout();
    this.setData({
      isLogin:false,
      user:{}
    })
  }
})