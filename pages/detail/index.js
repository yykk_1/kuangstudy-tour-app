var auth = require("../../utils/auth");

Page({
  data: {
    // 轮播图 
    swiperItems: [
      'http://i1.mifile.cn/f/i/2019/C76029A6/part6-2.jpg',
      'http://i1.mifile.cn/f/i/2019/C76029A6/part6-2.jpg',
      'http://i1.mifile.cn/f/i/2019/C76029A6/part6-2.jpg'
    ],
    // 切换导航
    navItems: ['商品详情', '相关评论'],
    active: 0,
    product: {
      name: "小米 MIX3 一面科技 一面艺术 ( 磁动力滑盖全面屏 | 故宫特别版 )",
      logo: "../../static/logo.png",
      price: 3188,
      priceMarket: 3200,
      imgs: [
        'https://img30.360buyimg.com/sku/jfs/t21838/18/2275707529/311540/cba1d04c/5b4f155fNac3aa2f0.jpg',
        'https://img30.360buyimg.com/sku/jfs/t22021/327/2281785192/48707/57806074/5b4f1579Nae7adb49.jpg',
        'https://img30.360buyimg.com/sku/jfs/t21682/256/2344553276/204456/cf7a2ddb/5b4ffbbfN48c54307.jpg'
      ]
    },
    // 模拟评论数据 (实际项目来自api请求)
    commentContents: [
      {
        "content": "故国三千里，深宫二十年。一声何满子，双泪落君前。",
        "name": "graceUI - 小码",
        "face": "https://graceui.oss-cn-beijing.aliyuncs.com/faces/1.png",
        "date": "08/10 08:00"
      },
      {
        "content": "而今渐行渐远，渐觉虽悔难追。漫寄消寄息，终久奚为。也拟重论缱绻，争奈翻覆思维。纵再会，只恐恩情，难似当时。",
        "name": "路过繁华",
        "face": "https://graceui.oss-cn-beijing.aliyuncs.com/faces/2.png",
        "date": "02/10 18:00"
      },
      {
        "content": "图片回复，点击图片可以预览......",
        "name": "林夕阳",
        "imgs": [
          "https://graceui.oss-cn-beijing.aliyuncs.com/scollimgs/4.png",
          "https://graceui.oss-cn-beijing.aliyuncs.com/scollimgs/3.png",
          "https://graceui.oss-cn-beijing.aliyuncs.com/scollimgs/1.png",
        ],
        "face": "https://graceui.oss-cn-beijing.aliyuncs.com/faces/2.png",
        "date": "08/12 09:00"
      }
    ],
    // 购买选择属性层展示
    attrIsShow: false,
    colorTips: [
      { name: '灰色', value: '灰色', checked: true },
      { name: '银色', value: '银色', checked: false }
    ],
    colorSelected: "灰色",
    typeTips: [
      { name: '套餐一', value: '套餐一', checked: true },
      { name: '套餐二', value: '套餐二', checked: false },
      { name: '套餐三', value: '套餐三', checked: false }
    ],
    typeSelected: "套餐一",
    buyNum: 1
  },
  // 分享
  share: function () { wx.showToast({ title: '请自行完善分享代码', icon: "none" }); },
  // 导航切换
  navChange: function (e) {
    this.setData({ active: e.detail });
  },
  // 评论图片展示
  showImgs: function (e) {
    console.log(e);
    var commentsIndex = e.currentTarget.dataset.index1;
    var imgIndex = e.currentTarget.dataset.index2;
    wx.previewImage({
      urls: this.data.commentContents[commentsIndex].imgs,
      current: this.data.commentContents[commentsIndex].imgs[imgIndex]
    })
  },
  //打开属性视图
  buyNow: function () { 
    if(!auth.isLogin()){
        wx.navigateTo({
          url: '../../pages/newlogin/index?flag=back'
        })

        return;
    }
    
    this.setData({ attrIsShow: true });
  },



  // 关闭属性
  closeAttr: function () { this.setData({ attrIsShow: false }); },
  // 颜色选择
  change1: function (e) { this.setData({ colorSelected: e.detail }); },
  // 类型选择
  change2: function (e) { this.setData({ typeSelected: e.detail }); },
  // 购买数量变化
  buyNumChange: function (e) {
    this.setData({ buyNum: e.detail[0] });
  },
  // 属性提交
  attrSubmit: function (e) {
    //后续操作
    wx.showToast({
      title: "属性已经收集,请观察控制台",
      icon: "none"
    });
    console.log("颜色 : " + this.data.colorSelected, "类型 : " + this.data.typeSelected, '数量 : ' + this.data.buyNum);
    //如果想关闭属性
    this.closeAttr();
  }
})