// 1: 导入service
import productService from '../../service/product/index';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    bannerHeight:315,
    bannerWidth: 710,
    searchKey: "",
    productList:[],
    pageNo:1,
    pageSize:10,
    pages:1,
    total:0,
    // 这个底部加载进度控制显示 0 初始化中 1 上拉加载更多 2 加载已完成 3 不显示
    loadingType: 0,
    loadingText:["更多精彩在后面...", "我在霸蛮的加载中...", "亲，我也是有底线的哦", '数据还在来的路上', ''],
  },

  taped : function(e){

    if(e.detail==2){
      this.setData({bannerHeight:500})
    }else{
      this.setData({bannerHeight:315})
    }
  },

  

  inputting: function (e) {
    console.log(e);
  },

  confirm: function (e) {
    console.log(e);
  },

  setKey: function (e) {
    var key = e.currentTarget.dataset.key;
    wx.showToast({
      title: '开始搜索 ' + key,
      icon: "none"
    });
    this.setData({
      searchKey: key
    });
  },

  removeAll: function () {
    this.setData({
      searchKeyHistory: []
    });
  },

  // 查询旅游线路产品
  loadProduct(callback){
    var {pageNo,pageSize,pages,total,categoryPid,productList} = this.data;
    if(pages > 0 && pageNo > pages ){
      wx.showToast({
        title: '加载完毕'
      });
      this.setData({loadingType:2});
      return;
    }

    productService.loadIndex({pageNo,pageSize,categoryPid}).then(res=>{
      var products = res.data.records;
      if(products && products.length > 0 ){
        products.map(res=>{
          if(res.tags && res.tags.length > 0){
            res.tags =res.tags.split(",")
          }
          return res; 
        }); 
      }

      var ltype = 0;
      if(res.data.total == 0) ltype = 3;
      if(res.data.total < pageSize ) ltype = 2;
      productList = productList.concat(products);
      this.setData({
        pages:res.data.pages,
        total:res.data.total,
        productList,
        pageNo:++pageNo,
        loadingType:ltype
      });

      callback && callback();
    });
  },

  toProduct(event){
     wx.setStorageSync('cpid', event.currentTarget.dataset.cpid);
     wx.switchTab({
       url: '../../pages/product/index',
     }) 
  },

  /************************************************************************* */
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 加载产品
    this.loadProduct(()=>{
      wx.stopPullDownRefresh({
        success: (res) => {},
      });
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   * 请在自己的index.json中进行堆"enablePullDownRefresh": true
   */
  onPullDownRefresh: function () {
     // 每次下拉刷新把轮播图的位置放在第一个去
     this.setData({currentIndex:0,productList:[],pageNo:1,pages:1,total:0,pageSize:10,loadingType:0});
     // 重新加载banner轮播图
     this.loadBanner();
     this.loadProduct();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
      if(this.data.loadingType == 2){
        return;
      }
      this.setData({loadingType:1});
      this.loadProduct();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    // webview 页面返回 webViewUrl
    return {
      title: 'YYKK小店'
      //,imageUrl: 'http://demo.png'
      //,query: 'name=xxx&age=xxx'
    }
  }
})