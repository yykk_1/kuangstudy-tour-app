/*
 * 学相伴小程序开源版 v1.1.0
 * Author: YYKK飞哥
 * Help document: https://www.kuangstudy.com/
 * gitee: https://gitee.com/xxxx
 * Copyright © 2020-2021 www.jiangqie.com All rights reserved.
 */
module.exports = {
	// 主域名
	DOMAIN: 'https://api.itbooking.net',
	//API的域名
	DOMAINAPI: 'https://api.itbooking.net/api'
};

