// 1： 导入appjs文件
var auth = require("./auth");

const SUCCESS_CODE = 200;

// 异步请求封装
// 参数1： url 异步请求地址
// 参数2： data 异步请求参数，是对象的方式存在
// 参数3： method 异步请求的方式，默认是：GET请求
// 参数4： headers 客户端发送给服务器端的请求的参数信息，也是以对象的方式存在
// 异步请求封装
var requestPromise = function(url,data={},method="GET",headers={}){
    // 1: 用promise进行封装返回
    return new Promise((resolve,reject)=>{
        
        // 2: 组装异步请求的接口的信息，这里就会获取token信息，传递给服务器 
        // todo: 可能要修改的位置
        var {token,openid} = auth.getUser();
        headers["Authorization"] = token;
        headers["ksd-user-name"] = openid;

        // 3： 发起异步请求 
        wx.request({
          url: url,
          data: data,
          method: method,
          dataType : "json",
          // 设置请求头的信息
          header:headers,
          // 执行成功函数
          success (res) {



            // 如果请求服务器成功的，返回状态200 
            if(res.data.code == SUCCESS_CODE){
                // 成功兑现执行resolve方法，如果你成功有then方法的话，就把resolve中数据延续到then方法中，
                resolve(res.data);
                return;
            }
            // 如果执行状态不是200，说明可能是验证失败的一些状态。 调用reject方法，如果你调用承诺的catch方法，就把reject参数传递给catch方法。

            console.log("------------------->",res)
            reject(1,res.data);
          },
          fail(err){
              // 此处是如果你服务器出现500错误，就会进入此方法
              tipmsg("服务器出现小小故障!!!");
              reject(1,err);
          },
          complete(){
            reject(2);
          }
        })
    });
}

// 异步请求的返回封装
var request = {
  
  // get请求
  get(url,data){
     return requestPromise(url,data,'GET',{});
  },

  // post请求
  post(url,data,contentType){
    switch(contentType){
        case "form" : 
            headers['content-type'] = 'application/x-www-form-urlencoded';
            break;
        case "json" : 
            headers['content-type'] = 'application/json';
            break;
        default :
            headers['content-type'] = 'application/x-www-form-urlencoded';
	}  
    return requestPromise(url,data,'POST',headers);
  }
}



// 消息提示框
function tipmsg(msg){
  wx.showToast({title:msg, icon:"none"});
}

function showLoading(title) {
  wx.showLoading({  title:title });
}


// 导入异步请求
export default request;