/*
 * 学相伴小程序开源版 v1.1.0
 * Author: YYKK飞哥
 * Help document: https://www.kuangstudy.com/
 * gitee: https://gitee.com/xxxx
 * Copyright © 2020-2021 www.jiangqie.com All rights reserved.
 */
const Config = require("./config.js");

// 组装URL地址
function makeURL(module, action) {
	return `${Config.DOMAINAPI}/v1/${module}/${action}`;
}

module.exports = {
  /**
   * 登录API
   */
  API_LOGIN: `${Config.DOMAIN}/wxlogin2`,

	/**
	 * 查询轮播图
	 */
  BANNER_HOME_API: makeURL('banner', 'list'), 

  /**
   * 查询产品
   *  */ 
  PRODUCT_HOME_API: makeURL('product', 'list'),

};
