module.exports = {
	// 控制版本信息
	KSD_VERSION: '1.1.0',
	// 用户信息
	KSD_USER_KEY: 'ksd_user',
	KSD_USER_KEY_OPENID: 'ksd_user_openid',
	KSD_USER_KEY_USERID: 'ksd_user_userid',
	KSD_USER_KEY_TOKEN: 'ksd_user_token'
};