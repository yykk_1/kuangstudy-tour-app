/*
 * 学相伴小程序开源版 v1.1.0
 * Author: YYKK飞哥
 * Help document: https://www.kuangstudy.com/
 * gitee: https://gitee.com/xxxx
 * Copyright © 2020-2021 www.jiangqie.com All rights reserved.
 * 使用方式：采用的是commonjs的语法 : 
 * var auth = require("../utils/auth.js");
 * 
 * 
 * 封装的思路:
 * 1、因为后续的很多的功能，都需要用户信息，比如：支付的时候，比如个人空间等都需要用户信息。
 * 2、如何去存储用户信息，才是公共的呢？就好比：java session 怎么做用户在各个页面进行共享呢？
 * 3、要达到很方便的退出登录，很方便的去获取用户信息，
 * 4、要进行集中管理用户信息 
 */

const Constant = require("./constants.js"); //获取TOKEN
const API = require("./api");
const app = getApp();


module.exports = {

  //检查登录态
  checkSession: function () {
    wx.checkSession({
      fail() {
        logout();
      }
    });
  },

  // 1：是否登录
  isLogin() {
    return this.getToken();
  },

  // 2： 获取用户信息
  getUser() {
     var userinfo  =  app.globalData.userinfo;
     if(!userinfo){
        userinfo =  wx.getStorageSync(Constant.KSD_USER_KEY);
     }
     return userinfo;
  },

  //3： 设置用户信息
  setUser: function (user) {
    // 缓存存一份
    wx.setStorageSync(Constant.KSD_USER_KEY, user);
    wx.setStorageSync(Constant.KSD_USER_KEY_TOKEN, user.token);
    wx.setStorageSync(Constant.KSD_USER_KEY_OPENID, user.openid);
    wx.setStorageSync(Constant.KSD_USER_KEY_USERID, user.userId);

    // 全存一份
    app.globalData.userinfo = user;
    app.globalData.token = user.token;
    app.globalData.openid = user.openid;
    app.globalData.userid = user.userid;
  },


  // 3: 退出登录
  logout() {
    wx.setStorageSync(Constant.KSD_USER_KEY, false);
    wx.removeStorageSync(Constant.KSD_USER_KEY_TOKEN);
    wx.removeStorageSync(Constant.KSD_USER_KEY_OPENID);
    wx.removeStorageSync(Constant.KSD_USER_KEY_USERID);
    app.globalData.userinfo = null;
    app.globalData.token = "";
    app.globalData.openid = "";
    app.globalData.userid = "";
  },


  //4：获取TOKEN
  getToken() {
    let user = wx.getStorageSync(Constant.KSD_USER_KEY);
		if (!user) {
			return false;
		}

    var token  =  app.globalData.token;
    if(!token){
      token =  user.token;
    }
   
    return token;
  },

  // 5: 远程注册用户信息，并且获取token以及用户信息
  loginWeixin: function (userinfo,flag="1") {
    var that = this;
    // 1：调用微信登陆的login方法获取code
    wx.login({
      success(res) {
        if (res.code) {
          wx.request({
            method: "GET",
            url: API.API_LOGIN,
            data: {
              code: res.code,
              ...userinfo
            },
            success: function (res) {

                

              if(res.data.code !=200){
                wx.showToast({title:"用户授权失败!", icon:"none"});
              }
              var userinfo  = res.data.data;
              // 如何将用户信息存储公共位置呢,
              
              
              // 放入缓存中一份
              that.setUser(userinfo);

              // 跳转指定页面或者首页
              if (flag == "1") {
                wx.switchTab({
                  url: '../../pages/index/index',
                })
              } else {
                wx.navigateBack({
                  delta: 1
                });
              }

            }
          })
        }
      }
    })


  },

  //6：判断用户是否已经授权
  weixinLogin(callback) {
      // 查看是否授权
      wx.getSetting({
        success (res){
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          if (res.authSetting['scope.userInfo']) {
            wx.getUserInfo({
              success: function(res) {
                callback && callback(res);
              }
            })
          }
        }
      })
  }

};