/**
 * @author yykk
 * @description:轮播图模块
 * @date 2021/08/10 22:32
 */
// 导入异步请求
import request from '../../utils/request'
// 导入API
const Config = require("../../utils/config.js");

export default {
  // 查询产品列表
  loadProduct({
    pageNo,
    pageSize,
    categoryPid = "",
    categoryCid = ""
  }) {
    return request.get(Config.DOMAINAPI + "/v1/product/list", {
      pageNo,
      pageSize,
      categoryPid,
      categoryCid
    });
  },

  // 查询产品列表
  loadIndex({
    pageNo,
    pageSize,
    categoryPid = "",
    categoryCid = ""
  }) {
    return request.get(Config.DOMAINAPI + "/v1/product/index", {
      pageNo,
      pageSize,
      categoryPid,
      categoryCid
    });
  },

  // 根据产品ID查询产品明细
  getProduct(productId) {
    return request.get(Config.DOMAINAPI + `/v1/product/get/${productId}`);
  }
};