/**
 * @author yykk
 * @description:轮播图模块
 * @date 2021/08/10 22:32
 */
// 导入异步请求
import request from '../../utils/request'
// 导入API
const Api = require("../../utils/api.js");

 export default {

    loadBanner(pageNo,pageSize){
      return request.get(Api.BANNER_HOME_API,{pageNo,pageSize});
    }
 };

