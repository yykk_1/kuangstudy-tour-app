/**
 * @author yykk
 * @description:轮播图模块
 * @date 2021/08/10 22:32
 */
// 导入异步请求
import request from '../../utils/request'
// 导入API
const Config = require("../../utils/config.js");

export default {
  // 查询一级分类信息
  findCategoryRoot() {
    return request.get(Config.DOMAINAPI + '/v1/category/root');
  }
};